########################################################################
# genPythonVEnv.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

#
# CMake macro: generate custom targets to create Python virtual environments
#
macro(genPythonVEnv)

  find_package(Python3 QUIET)
  if(Python3_EXECUTABLE)
    
    if(NOT DEFINED VENV_DIR)
      set(PYTHON_VENV_DIR ${PROJECT_BINARY_DIR})
    endif()

    # AQASM
    if(LIBKET_WITH_AQASM)
      if(DEFINED MYQLM_VERSION)
        list(APPEND PYTHON_PACKAGES "myqlm==${MYQLM_VERSION}")
      else()
        list(APPEND PYTHON_PACKAGES "myqlm")
      endif()
    endif()
    
    # Cirq
    if(LIBKET_WITH_CIRQ)
      if(DEFINED CIRQ_VERSION)
        list(APPEND PYTHON_PACKAGES "cirq==${CIRQ_VERSION}")
      else()
        list(APPEND PYTHON_PACKAGES "cirq")
      endif()
    endif()
    
    # Quantum Inspire
    if(LIBKET_WITH_CQASM)
      if(DEFINED QUANTUMINSPIRE_VERSION)
        list(APPEND PYTHON_PACKAGES "quantuminspire==${QUANTUMINSPIRE_VERSION}")
      else()
        list(APPEND PYTHON_PACKAGES "quantuminspire")
      endif()
      # matplotlib as dependency
      if(DEFINED MATPLOTLIB_VERSION)
        list(APPEND PYTHON_PACKAGES "matplotlib==${MATPLOTLIB_VERSION}")
      else()
        list(APPEND PYTHON_PACKAGES "matplotlib")
      endif()
    endif()
    
    # Qiskit
    if(LIBKET_WITH_OPENQASM)
      if(DEFINED QISKIT_VERSION)
        list(APPEND PYTHON_PACKAGES "qiskit==${QISKIT_VERSION}")
      else()
        list(APPEND PYTHON_PACKAGES "qiskit")
      endif()
      # matplotlib as dependency
      if(DEFINED MATPLOTLIB_VERSION)
        list(APPEND PYTHON_PACKAGES "matplotlib==${MATPLOTLIB_VERSION}")
      else()
        list(APPEND PYTHON_PACKAGES "matplotlib")
      endif()
      # pylatexenc as dependency
      if(DEFINED PYLATEXENC_VERSION)
        list(APPEND PYTHON_PACKAGES "pylatexenc==${PYLATEXENC_VERSION}")
      else()
        list(APPEND PYTHON_PACKAGES "pylatexenc")
      endif()
    endif()

    # PennyLany
    if(LIBKET_WITH_PENNYLANE)
      if(DEFINED PENNYLANE_VERSION)
        list(APPEND PYTHON_PACKAGES "pennylane==${PENNYLANE_VERSION}")
      else()
        list(APPEND PYTHON_PACKAGES "pennylane")
      endif()
    endif()
    
    # PyQuil
    if(LIBKET_WITH_QUIL)
      if(DEFINED PYQUIL_VERSION)
        list(APPEND PYTHON_PACKAGES "pyquil==${PYQUIL_VERSION}")
      else()
        list(APPEND PYTHON_PACKAGES "pyquil")
      endif()
      message(STATUS "Don't forget to install the Forest SDK from https://qcs.rigetti.com/sdk-downloads" )
    endif()

    # Remove duplicate packages
    list(REMOVE_DUPLICATES PYTHON_PACKAGES)

    # Create requirements.txt file
    file(WRITE "${PROJECT_BINARY_DIR}/requirements.txt" "")
    foreach(package ${PYTHON_PACKAGES})
      file(APPEND "${PROJECT_BINARY_DIR}/requirements.txt" "${package}\n")
    endforeach()

    message(STATUS "Generating requirements.txt file done")
    
    # Create custom target to install Python virtual environment
    add_custom_target(install-python-packages-venv
      COMMAND ${CMAKE_COMMAND} -E make_directory ${PYTHON_VENV_DIR}/venv/libket
      COMMAND ${Python3_EXECUTABLE} -m venv ${PYTHON_VENV_DIR}/venv/libket
      COMMAND ${PYTHON_VENV_DIR}/venv/libket/bin/pip3 install --upgrade pip setuptools
      COMMAND ${PYTHON_VENV_DIR}/venv/libket/bin/pip3 install -r ${PROJECT_BINARY_DIR}/requirements.txt
      COMMENT "Activate virtual Python environment by running source ${PYTHON_VENV_DIR}/venv/libket/bin/activate"
      )
    add_custom_target(install-python-packages
      COMMAND pip3 install -r ${PROJECT_BINARY_DIR}/requirements.txt
      )

    message(STATUS "Install Python packages in virtual environment by running 'make install-python-packages-venv'")
    message(STATUS "Install Python packages in global environment by running 'make install-python-packages'")
  endif() 
endmacro()
