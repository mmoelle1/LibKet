########################################################################
# deps.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

include(FetchContent)

########################################################################
# Armadillo
########################################################################

FetchContent_Declare(armadillo
  URL http://sourceforge.net/projects/arma/files/armadillo-12.4.1.tar.xz
  )

FetchContent_MakeAvailable(armadillo)
FetchContent_GetProperties(armadillo)
include_directories("${armadillo_SOURCE_DIR}/include")
list(APPEND LIBKET_C_TARGET_LINK_LIBRARIES   armadillo)
list(APPEND LIBKET_CXX_TARGET_LINK_LIBRARIES armadillo)

########################################################################
# nlohmann JSON
########################################################################

if(NOT LIBKET_WITH_OPENQL) # OpenQL contains this library
  include(FetchContent)
  FetchContent_Declare(nlohmann_json
    URL https://github.com/nlohmann/json/archive/refs/tags/v3.11.2.zip
    )
  FetchContent_MakeAvailable(nlohmann_json)
  FetchContent_GetProperties(nlohmann_json)
  include_directories(${nlohmann_json_SOURCE_DIR}/single_include)
endif()

########################################################################
# NLopt
########################################################################

FetchContent_Declare(nlopt
  URL https://github.com/stevengj/nlopt/archive/refs/tags/v2.7.1.tar.gz
  PATCH_COMMAND patch ${CMAKE_BINARY_DIR}/_deps/nlopt-src/CMakeLists.txt < ${CMAKE_SOURCE_DIR}/cmake/nlopt.patch
  )

FetchContent_MakeAvailable(nlopt)
FetchContent_GetProperties(nlopt)
include_directories("${nlopt_SOURCE_DIR}/include")
list(APPEND LIBKET_C_TARGET_LINK_LIBRARIES   nlopt)
list(APPEND LIBKET_CXX_TARGET_LINK_LIBRARIES nlopt)

########################################################################
# OptimLib
########################################################################

include_directories("${PROJECT_SOURCE_DIR}/external/optim")

########################################################################
# PEGTL
########################################################################

FetchContent_Declare(pegtl
  URL https://github.com/taocpp/PEGTL/archive/refs/tags/3.2.7.zip
  )

FetchContent_MakeAvailable(pegtl)
FetchContent_GetProperties(pegtl)
include_directories("${pegtl_SOURCE_DIR}/include")

########################################################################
# PyBind11 + PyBind11-JSON
########################################################################

if (LIBKET_BUILD_PYTHON_API)
  FetchContent_Declare(pybind11
    URL https://github.com/pybind/pybind11/archive/refs/heads/master.zip
    FIND_PACKAGE_ARGS
    )
  FetchContent_MakeAvailable(pybind11)
  FetchContent_GetProperties(pybind11)
  include_directories(${pybind11_SOURCE_DIR}/include)
  
  FetchContent_Declare(pybind11_json
    URL https://github.com/pybind/pybind11_json/archive/refs/heads/master.zip
    FIND_PACKAGE_ARGS
    )
  FetchContent_Populate(pybind11_json)
  FetchContent_GetProperties(pybind11_json)
  include_directories(${pybind11_json_SOURCE_DIR}/include)  
endif()

########################################################################
# Universal
########################################################################

# FetchContent_Declare(
#   universal
#   URL https://github.com/stillwater-sc/universal/archive/refs/tags/v3.53.zip
#   )

# FetchContent_GetProperties(universal)
# if(NOT universal_POPULATED)
#   message(STATUS "  Universal")
#   FetchContent_Populate(universal)
# endif()
