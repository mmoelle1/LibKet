/** @file python_api/PyQJob.hpp

    @brief Python API quantum job execution class

    @copyright This file is part of the LibKet library (Python API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef PYTHON_API_QJOB_HPP
#define PYTHON_API_QJOB_HPP

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "PyQBase.hpp"

/**
   @brief Quantum job execution class

   @ingroup py_api
*/
class PyQJob : public PyQBase
{};

/**
   @brief Creates quantum job execution Python module

   @ingroup py_api
*/
void
pybind11_init_job(pybind11::module& m);

#endif // PYTHON_API_QJOB_HPP
