/** @file libket/gates/QGate.hpp

    @brief C++ API: Quantum gate classes

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup unarygates Unary gates
    @ingroup  gates
    
    @defgroup binarygates Binary gates
    @ingroup  gates
    
    @defgroup ternarygates Ternary gates
    @ingroup  gates
 */

#pragma once
#ifndef QGATE_HPP
#define QGATE_HPP

#include <gates/QGate.h>
#include <QBase.hpp>
#include <QExpression.hpp>
#include <QFilter.hpp>

#include <list>

namespace LibKet {

/** @namespace LibKet::gates

    @brief
    The LibKet::gates namespace, containing all gates of the LibKet project

    The LibKet::gates namespace contains all gates of the LibKet
    project that is exposed to the end-user. All functionality in
    this namespace has a stable API over future LibKet releases.
 */
namespace gates {

/** @namespace LibKet::gates::detail

    @brief
    The LibKet::gates:detail namespace, containing implementation details.

    The LibKet::gates::detail namespace contains internal
    implementation details of the gates of the LibKet project that are
    not exposed to the end-user. Functionality in this namespace can
    change without notice.
 */
namespace detail {}

/**
@brief Quantum gate base class

The Quantum gate base class is the base class of all Quantum
gate classes.

@ingroup gates
*/
class QGate : public QBase
{};

/**
@brief Unary quantum gate class

The LibKet unary quantum gate class is a wrapper for all unary quantum gates.

@ingroup  unarygates
*/
template<typename _expr, typename _gate, typename _filter = filters::QFilter>
class UnaryQGate : public QGate
{
public:
  /// @brief Quantum expression type
  using expr_t = typename std::decay<_expr>::type;

  /// @brief Quantum expression
  const expr_t expr;

  /// @brief Quantum gate type
  using gate_t = typename std::decay<_gate>::type;

  /// @brief Quantum filter type
  using filter_t = typename std::decay<_filter>::type;

  /// @brief Constructor
  constexpr UnaryQGate()
    : expr(typename std::decay<expr_t>::type{})
  {}

  /// @brief Constructor
  constexpr UnaryQGate(_expr expr)
    : expr(expr)
  {}

  /// @brief Constructor
  template<typename __filter>
  constexpr UnaryQGate(const UnaryQGate<_expr, _gate, __filter>& gate)
    : expr(gate.expr)
  {}

  /// @brief Constructor
  template<typename __filter>
  constexpr UnaryQGate(UnaryQGate<_expr, _gate, __filter>&& gate)
    : expr(gate.expr)
  {}

  /// @brief Operator()
  /// @note  no arguments
  inline constexpr auto operator()()
  {
    return *this;
  }
  
  /// @brief Operator()
  /// @note  specialization for QExpression objects with QFilter expression
  template<std::size_t _qubits, QBackendType _qbackend, typename __expr = _expr>
  inline constexpr auto operator()(QExpression<_qubits, _qbackend>& expr)
    const noexcept -> typename std::enable_if<
      std::is_base_of<filters::QFilter,
                      typename std::decay<__expr>::type>::value,
      decltype(expr)>::type&
  {
    return _gate::template apply<_qubits,
                                 typename std::decay<__expr>::type::filter_t>(
      expr);
  }

  /// @brief Operator()
  /// @note  specialization for QExpression objects with QGate expression
  template<std::size_t _qubits, QBackendType _qbackend, typename __expr = _expr>
  inline constexpr auto operator()(QExpression<_qubits, _qbackend>& expr)
    const noexcept -> typename std::enable_if<
      std::is_base_of<QGate, typename std::decay<__expr>::type>::value,
      decltype(expr)>::type&
  {
    return _gate::template apply<_qubits,
                                 typename std::decay<__expr>::type::filter_t>(
      this->expr(expr));
  }

  /// @brief Operator()
  /// @note  specialization for QFilterTag objects
  template<std::size_t _tag, typename __filter, typename __tagged_filter>
  inline constexpr auto operator()(
    const filters::QFilterTag<_tag, __filter, __tagged_filter>&) const noexcept
  {
    return UnaryQGate<_expr,
                      _gate,
                      filters::QFilterTag<_tag, _filter, _filter>>(this->expr);
  }

  /// @brief Operator()
  /// @note  specialization for QFilterGotoTag objects
  template<std::size_t _tag, typename __filter>
  inline constexpr auto operator()(
    const filters::QFilterGotoTag<_tag, __filter>&) const noexcept
  {
    return UnaryQGate<_expr, _gate, filters::QFilterGotoTag<_tag, _filter>>(
      this->expr);
  }

  /// @brief Operator()
  /// @note  specialization for generic expressions
  template<typename __expr>
  inline constexpr auto operator()(const __expr& expr) const noexcept
  {
    return UnaryQGate<decltype(typename std::decay<_expr>::type{}(expr)),
                      _gate,
                      decltype(typename filters::getFilter<_expr>::type{}(
                        typename std::decay<__expr>::type::filter_t{}))>(expr);
  }

  /// @brief Prints the abstract syntax tree
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string
  template<std::size_t level = 1>
  std::string show() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::show;
    show<level>(*this, os);
    return os.str();
  }

  /// @brief Prints the abstract syntax tree (DOT-language)
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string (DOT-language)
  template<std::size_t level = 1>
  std::string dot() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::dot;
    dot<level>(*this, os);
    return os.str();
  }

  /// @brief Returns adjoint gate
  ///
  /// @result expression representing the adjoint gate
  auto dagger() const noexcept
  {    
    using ::LibKet::dagger;
    return dagger(*this);
  }
};

/**
 @brief Serialize operation
*/
template<typename _expr, typename _gate, typename _filter>
std::ostream&
operator<<(std::ostream& os, const UnaryQGate<_expr, _gate, _filter>& gate)
{  
  // Check if filter chain has changed from previous sub-expression
  if (std::is_same<
      typename std::decay<decltype(gate)>::type::filter_t,
      typename std::decay<decltype(gate)>::type::expr_t::filter_t>::value) {
    // Output gate and next sub-expression
    os << typename std::decay<decltype(gate)>::type::gate_t()
       << gate.expr << ")";
  } else {
    // Output filter chain, gate and next sub-expression
    os << set
       << typename std::decay<decltype(gate)>::type::filter_t()
       << unset
       << typename std::decay<decltype(gate)>::type::gate_t()
       << gate.expr << "))";
  }
  return os;
}

/**
   @brief Prints the abstract syntax tree of the expression

   @note  specialization for UnaryQGate objects
*/
template<std::size_t level = 1,
         typename _expr,
         typename _gate,
         typename _filter>
inline static auto
show(const UnaryQGate<_expr, _gate, _filter>& gate,
     std::ostream& os = std::cout,
     const std::string& prefix = "")
{
  os << "UnaryQGate\n";
  if (level > 0) {
    os << prefix << "|   gate = ";
    show<level - 1>(_gate{}, os, prefix + "|          ");
    os << prefix << "| filter = ";
    show<level - 1>(_filter{}, os, prefix + "|          ");
    os << prefix << "|   expr = ";
    show<level - 1>(gate.expr, os, prefix + "|          ");
  }

  return gate;
}

/**
   @brief Prints the abstract syntax tree of the expression (DOT-language)

   @note  specialization for UnaryQGate objects
*/
template<std::size_t level = 1,
         typename _expr,
         typename _gate,
         typename _filter>
inline static auto
dot(const UnaryQGate<_expr, _gate, _filter>& gate,
    std::ostream& os = std::cout)
{
  os << "UnaryQGate\n";
  
  return gate;
}

/**
   @brief Creates the adjoint gate

   @note  specialization for UnaryQGate objects
*/
template<typename _expr,
         typename _gate,
         typename _filter>
inline static auto
dagger(const UnaryQGate<_expr, _gate, _filter>& gate)
{
  return UnaryQGate<
    decltype(dagger(_expr{})),
    decltype(dagger(_gate{})),
    _filter>{};
}

/**
   @brief Returns the list of variables in the expression

   @note  specialization for UnaryQGate objects
*/
template<typename _expr,
         typename _gate,
         typename _filter>
inline static auto
vars(const UnaryQGate<_expr, _gate, _filter>& gate)
{
  std::list<std::string> list;
  
  return list;
}
  
/**
@brief Binary quantum gate class

The LibKet binary quantum gate class is a wrapper for all binary quantum
gates.

@ingroup binarygates
*/
template<typename _expr0,
         typename _expr1,
         typename _gate,
         typename _filter = filters::QFilter>
class BinaryQGate : public QGate
{
public:
  /// @{
  /// @brief Quantum expression types
  using expr0_t = typename std::decay<_expr0>::type;
  using expr1_t = typename std::decay<_expr1>::type;
  /// @}

  /// @{
  /// @brief Quantum expressions
  const expr0_t expr0;
  const expr1_t expr1;
  /// @}
  
  /// @brief Quantum gate type
  using gate_t = typename std::decay<_gate>::type;

  /// @brief Quantum filter type
  using filter_t = typename std::decay<_filter>::type;

  /// @brief Constructor
  constexpr BinaryQGate()
    : expr0(typename std::decay<expr0_t>::type{})
    , expr1(typename std::decay<expr1_t>::type{})
  {}

  /// @brief Constructor
  constexpr BinaryQGate(_expr0 expr0, _expr1 expr1)
    : expr0(expr0)
    , expr1(expr1)
  {}

  /// @brief Constructor
  template<typename __filter>
  constexpr BinaryQGate(
    const BinaryQGate<_expr0, _expr1, _gate, __filter>& gate)
    : expr0(gate.expr0)
    , expr1(gate.expr1)
  {}

  /// @brief Constructor
  template<typename __filter>
  constexpr BinaryQGate(BinaryQGate<_expr0, _expr1, _gate, __filter>&& gate)
    : expr0(gate.expr0)
    , expr1(gate.expr1)
  {}

  /// @brief Operator()
  inline constexpr auto operator()()
  {
    return *this;
  }
  
  /// @brief Operator()
  /// @note  specialization for QExpression objects with two QFilter expressions
  template<std::size_t _qubits,
           QBackendType _qbackend,
           typename __expr0 = _expr0,
           typename __expr1 = _expr1>
  inline constexpr auto operator()(QExpression<_qubits, _qbackend>& expr)
    const noexcept -> typename std::enable_if<
      std::is_base_of<filters::QFilter,
                      typename std::decay<__expr0>::type>::value &&
        std::is_base_of<filters::QFilter,
                        typename std::decay<__expr1>::type>::value,
      decltype(expr)>::type&
  {
    return _gate::template apply<_qubits,
                                 typename std::decay<__expr0>::type::filter_t,
                                 typename std::decay<__expr1>::type::filter_t>(
      expr);
  }

  /// @brief Operator()
  /// @note  specialization for QExpression objects with one QFilter and one QGate expression
  template<std::size_t _qubits,
           QBackendType _qbackend,
           typename __expr0 = _expr0,
           typename __expr1 = _expr1>
  inline constexpr auto operator()(QExpression<_qubits, _qbackend>& expr)
    const noexcept -> typename std::enable_if<
      std::is_base_of<filters::QFilter,
                      typename std::decay<__expr0>::type>::value &&
        std::is_base_of<QGate, typename std::decay<__expr1>::type>::value,
      decltype(expr)>::type&
  {
    return _gate::template apply<_qubits,
                                 typename std::decay<__expr0>::type::filter_t,
                                 typename std::decay<__expr1>::type::filter_t>(
      expr1(expr));
  }

  /// @brief Operator()
  /// @note  specialization for QExpression objects with one QGate and one QFilter expression
  template<std::size_t _qubits,
           QBackendType _qbackend,
           typename __expr0 = _expr0,
           typename __expr1 = _expr1>
  inline constexpr auto operator()(QExpression<_qubits, _qbackend>& expr)
    const noexcept -> typename std::enable_if<
      std::is_base_of<QGate, typename std::decay<__expr0>::type>::value &&
        std::is_base_of<filters::QFilter,
                        typename std::decay<__expr1>::type>::value,
      decltype(expr)>::type&
  {
    return _gate::template apply<_qubits,
                                 typename std::decay<__expr0>::type::filter_t,
                                 typename std::decay<__expr1>::type::filter_t>(
      expr0(expr));
  }

  /// @brief Operator()
  /// @note  specialization for QExpression objects with two QGate expressions
  template<std::size_t _qubits,
           QBackendType _qbackend,
           typename __expr0 = _expr0,
           typename __expr1 = _expr1>
  inline constexpr auto operator()(QExpression<_qubits, _qbackend>& expr)
    const noexcept -> typename std::enable_if<
      std::is_base_of<QGate, typename std::decay<__expr0>::type>::value &&
        std::is_base_of<QGate, typename std::decay<__expr1>::type>::value,
      decltype(expr)>::type&
  {
    return _gate::template apply<_qubits,
                                 typename std::decay<__expr0>::type::filter_t,
                                 typename std::decay<__expr1>::type::filter_t>(
#ifdef LIBKET_L2R_EVALUATION
      expr1(expr0(expr))
#else
      expr0(expr1(expr))
#endif
    );
  }

  /// @brief Operator()
  /// @note  specialization for QFilterTag objects
  template<std::size_t _tag, typename __filter, typename __tagged_filter>
  inline constexpr auto operator()(
    const filters::QFilterTag<_tag, __filter, __tagged_filter>&) const noexcept
  {
    return BinaryQGate<_expr0,
                       _expr1,
                       _gate,
                       filters::QFilterTag<_tag, _filter, _filter>>(
      this->expr0, this->expr1);
  }

  /// @brief Operator()
  /// @note  specialization for QFilterGotoTag objects
  template<std::size_t _tag, typename __filter>
  inline constexpr auto operator()(
    const filters::QFilterGotoTag<_tag, __filter>&) const noexcept
  {
    return BinaryQGate<_expr0,
                       _expr1,
                       _gate,
                       filters::QFilterGotoTag<_tag, _filter>>(this->expr0,
                                                               this->expr1);
  }

  /// @brief Operator()
  /// @note  specialization for generic expressions
  template<typename __expr>
  inline constexpr auto operator()(const __expr& expr) const noexcept
  {
    return BinaryQGate<decltype(typename std::decay<_expr0>::type{}(expr)),
                       decltype(typename std::decay<_expr1>::type{}(expr)),
                       _gate,
                       decltype(
                         typename filters::getFilter<_expr0>::type{}(
                           typename std::decay<__expr>::type::filter_t{})
                         << typename filters::getFilter<_expr1>::type{}(
                              typename std::decay<__expr>::type::filter_t{}))>(
      expr, expr);
  }

  /// @brief Operator()
  /// @note  specialization for generic expressions
  template<typename __expr0, typename __expr1>
  inline constexpr auto operator()(const __expr0& expr0,
                                   const __expr1& expr1) const noexcept
  {
    return BinaryQGate<decltype(typename std::decay<_expr0>::type{}(expr0)),
                       decltype(typename std::decay<_expr1>::type{}(expr1)),
                       _gate,
                       decltype(
                         typename filters::getFilter<_expr0>::type{}(
                           typename std::decay<__expr0>::type::filter_t{})
                         << typename filters::getFilter<_expr1>::type{}(
                              typename std::decay<__expr1>::type::filter_t{}))>(
      expr0, expr1);
  }


  /// @brief Prints the abstract syntax tree
  ///
  /// @tparam level specifies the number of levels to print
  /// @result       AST as a string
  template<std::size_t level = 1>
  std::string show() const noexcept
  {
    std::ostringstream os;
    using ::LibKet::show;
    show<level>(*this, os);
    return os.str();
  }

  /// @brief Prints the abstract syntax tree (DOT-language)
  ///
  /// @tparam level specifies the number of levels to print
  /// @result       AST as a string (DOT-language)
  template<std::size_t level = 1>
  std::string dot() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::dot;
    dot<level>(*this, os);
    return os.str();
  }

  /// @brief Returns adjoint gate
  ///
  /// @result expression representing the adjoint gate
  auto dagger() const noexcept
  {    
    using ::LibKet::dagger;
    return dagger(*this);
  }
};

/**
 @brief Serialize operation
*/
template<typename _expr0, typename _expr1, typename _gate, typename _filter>
std::ostream&
operator<<(std::ostream& os,
           const BinaryQGate<_expr0, _expr1, _gate, _filter>& gate)
{
  // Check if filter chain has changed from previous sub-expression
  if (std::is_same<
        typename std::decay<decltype(gate)>::type::filter_t,
        decltype(typename std::decay<decltype(gate)>::type::expr0_t::filter_t{}
                 << typename std::decay<decltype(
                      gate)>::type::expr1_t::filter_t{})>::value) {
    // Output gate and next two sub-expressions
    os << typename std::decay<decltype(gate)>::type::gate_t()
       << gate.expr0 << "," << gate.expr1 << ")";
  } else {
    // Output filter chain, gate and next two sub-expressions
    os << set
       << typename std::decay<decltype(gate)>::type::filter_t()
       << unset
       << typename std::decay<decltype(gate)>::type::gate_t()
       << gate.expr0 << "," << gate.expr1 << "))";
  }
  return os;
}

/**
   @brief Prints the abstract syntax tree of the expression

   @note  specialization for BinaryQGate objects
*/
template<std::size_t level = 1,
         typename _expr0,
         typename _expr1,
         typename _gate,
         typename _filter>
inline static auto
show(const BinaryQGate<_expr0, _expr1, _gate, _filter>& gate,
     std::ostream& os = std::cout,
     const std::string& prefix = "")
{
  os << "BinaryQGate\n";
  if (level > 0) {
    os << prefix << "|   gate = ";
    show<level - 1>(_gate{}, os, prefix + "|          ");
    os << prefix << "| filter = ";
    show<level - 1>(_filter{}, os, prefix + "|          ");
    os << prefix << "|  expr0 = ";
    show<level - 1>(gate.expr0, os, prefix + "|          ");
    os << prefix << "|  expr1 = ";
    show<level - 1>(gate.expr1, os, prefix + "|          ");
  }

  return gate;
}

/**
   @brief Prints the abstract syntax tree of the expression (DOT-language)

   @note  specialization for BinaryQGate objects
*/
template<std::size_t level = 1,
         typename _expr0,
         typename _expr1,
         typename _gate,
         typename _filter>
inline static auto
dot(const BinaryQGate<_expr0, _expr1, _gate, _filter>& gate,
    std::ostream& os = std::cout)
{
  os << "BinaryQGate\n";
  
  return gate;
}

/**
   @brief Creates the adjoint gate

   @note  specialization for BinaryQGate objects
*/
template<typename _expr0,
         typename _expr1,
         typename _gate,
         typename _filter>
inline static auto
dagger(const BinaryQGate<_expr0, _expr1, _gate, _filter>& gate)
{
  return BinaryQGate<
    decltype(dagger(_expr0{})),
    decltype(dagger(_expr1{})),
    decltype(dagger(_gate{})),
    _filter>{};
}

/**
@brief Ternary quantum gate class

The LibKet ternary quantum gate class is a wrapper for all ternary quantum
gates.

@ingroup ternarygates
*/
template<typename _expr0,
         typename _expr1,
         typename _expr2,
         typename _gate,
         typename _filter = filters::QFilter>
class TernaryQGate : public QGate
{
public:
  /// @{
  /// @brief Quantum expression types
  using expr0_t = typename std::decay<_expr0>::type;
  using expr1_t = typename std::decay<_expr1>::type;
  using expr2_t = typename std::decay<_expr2>::type;
  /// @}

  /// @{
  /// Quantum expressions
  const expr0_t expr0;
  const expr1_t expr1;
  const expr2_t expr2;
  /// @}

  /// @brief Quantum gate type
  using gate_t = typename std::decay<_gate>::type;

  /// @brief Quantum filter type
  using filter_t = typename std::decay<_filter>::type;

  /// @brief Constructor
  constexpr TernaryQGate()
    : expr0(typename std::decay<expr0_t>::type{})
    , expr1(typename std::decay<expr1_t>::type{})
    , expr2(typename std::decay<expr2_t>::type{})
  {}

  /// @brief Constructor
  constexpr TernaryQGate(_expr0 expr0, _expr1 expr1, _expr2 expr2)
    : expr0(expr0)
    , expr1(expr1)
    , expr2(expr2)
  {}

  /// @brief Constructor
  template<typename __filter>
  constexpr TernaryQGate(
    const TernaryQGate<_expr0, _expr1, _expr2, _gate, __filter>& gate)
    : expr0(gate.expr0)
    , expr1(gate.expr1)
    , expr2(gate.expr2)
  {}

  /// @brief Constructor
  template<typename __filter>
  constexpr TernaryQGate(
    TernaryQGate<_expr0, _expr1, _expr2, _gate, __filter>&& gate)
    : expr0(gate.expr0)
    , expr1(gate.expr1)
    , expr2(gate.expr2)
  {}

  /// @brief Operator()
  inline constexpr auto operator()()
  {
    return *this;
  }
  
  /// @brief Operator()
  /// @note  specialization for QExpression objects with three QFilter expressions
  template<std::size_t _qubits,
           QBackendType _qbackend,
           typename __expr0 = _expr0,
           typename __expr1 = _expr1,
           typename __expr2 = _expr2>
  inline constexpr auto operator()(QExpression<_qubits, _qbackend>& expr)
    const noexcept -> typename std::enable_if<
      std::is_base_of<filters::QFilter,
                      typename std::decay<__expr0>::type>::value &&
        std::is_base_of<filters::QFilter,
                        typename std::decay<__expr1>::type>::value &&
        std::is_base_of<filters::QFilter,
                        typename std::decay<__expr2>::type>::value,
      decltype(expr)>::type&
  {
    return _gate::template apply<_qubits,
                                 typename std::decay<__expr0>::type::filter_t,
                                 typename std::decay<__expr1>::type::filter_t,
                                 typename std::decay<__expr2>::type::filter_t>(
      expr);
  }

  /// @brief Operator()
  /// @note  specialization for QExpression objects with two QFilter and one QGate expression
  template<std::size_t _qubits,
           QBackendType _qbackend,
           typename __expr0 = _expr0,
           typename __expr1 = _expr1,
           typename __expr2 = _expr2>
  inline constexpr auto operator()(QExpression<_qubits, _qbackend>& expr)
    const noexcept -> typename std::enable_if<
      std::is_base_of<filters::QFilter,
                      typename std::decay<__expr0>::type>::value &&
        std::is_base_of<filters::QFilter,
                        typename std::decay<__expr1>::type>::value &&
        std::is_base_of<QGate, typename std::decay<__expr2>::type>::value,
      decltype(expr)>::type&
  {
    return _gate::template apply<_qubits,
                                 typename std::decay<__expr0>::type::filter_t,
                                 typename std::decay<__expr1>::type::filter_t,
                                 typename std::decay<__expr2>::type::filter_t>(
      expr2(expr));
  }

  /// @brief Operator()
  /// @note specialization for QExpression objects with one Filter, one QGate and one QFilter expression
  template<std::size_t _qubits,
           QBackendType _qbackend,
           typename __expr0 = _expr0,
           typename __expr1 = _expr1,
           typename __expr2 = _expr2>
  inline constexpr auto operator()(QExpression<_qubits, _qbackend>& expr)
    const noexcept -> typename std::enable_if<
      std::is_base_of<filters::QFilter,
                      typename std::decay<__expr0>::type>::value &&
        std::is_base_of<QGate, typename std::decay<__expr1>::type>::value &&
        std::is_base_of<filters::QFilter,
                        typename std::decay<__expr2>::type>::value,
      decltype(expr)>::type&
  {
    return _gate::template apply<_qubits,
                                 typename std::decay<__expr0>::type::filter_t,
                                 typename std::decay<__expr1>::type::filter_t,
                                 typename std::decay<__expr2>::type::filter_t>(
      expr1(expr));
  }

  /// @brief Operator()
  /// @note specialization for QExpression objects with one QFilter and two QGate expressions
  template<std::size_t _qubits,
           QBackendType _qbackend,
           typename __expr0 = _expr0,
           typename __expr1 = _expr1,
           typename __expr2 = _expr2>
  inline constexpr auto operator()(QExpression<_qubits, _qbackend>& expr)
    const noexcept -> typename std::enable_if<
      std::is_base_of<filters::QFilter,
                      typename std::decay<__expr0>::type>::value &&
        std::is_base_of<QGate, typename std::decay<__expr1>::type>::value &&
        std::is_base_of<QGate, typename std::decay<__expr2>::type>::value,
      decltype(expr)>::type&
  {
    return _gate::template apply<_qubits,
                                 typename std::decay<__expr0>::type::filter_t,
                                 typename std::decay<__expr1>::type::filter_t,
                                 typename std::decay<__expr2>::type::filter_t>(
#ifdef LIBKET_L2R_EVALUATION
      expr2(expr1(expr))
#else
      expr1(expr2(expr))
#endif
    );
  }

  /// @brief Operator()
  /// @note  specialization for QExpression objects with one QGate and two QFilter expressions
  template<std::size_t _qubits,
           QBackendType _qbackend,
           typename __expr0 = _expr0,
           typename __expr1 = _expr1,
           typename __expr2 = _expr2>
  inline constexpr auto operator()(QExpression<_qubits, _qbackend>& expr)
    const noexcept -> typename std::enable_if<
      std::is_base_of<QGate, typename std::decay<__expr0>::type>::value &&
        std::is_base_of<filters::QFilter,
                        typename std::decay<__expr1>::type>::value &&
        std::is_base_of<filters::QFilter,
                        typename std::decay<__expr2>::type>::value,
      decltype(expr)>::type&
  {
    return _gate::template apply<_qubits,
                                 typename std::decay<__expr0>::type::filter_t,
                                 typename std::decay<__expr1>::type::filter_t,
                                 typename std::decay<__expr2>::type::filter_t>(
      expr0(expr));
  }

  /// @brief Operator()
  /// @note  specialization for QExpression objects with one QGate, one QFilter, and one QGate expression
  template<std::size_t _qubits,
           QBackendType _qbackend,
           typename __expr0 = _expr0,
           typename __expr1 = _expr1,
           typename __expr2 = _expr2>
  inline constexpr auto operator()(QExpression<_qubits, _qbackend>& expr)
    const noexcept -> typename std::enable_if<
      std::is_base_of<QGate, typename std::decay<__expr0>::type>::value &&
        std::is_base_of<filters::QFilter,
                        typename std::decay<__expr1>::type>::value &&
        std::is_base_of<QGate, typename std::decay<__expr2>::type>::value,
      decltype(expr)>::type&
  {
    return _gate::template apply<_qubits,
                                 typename std::decay<__expr0>::type::filter_t,
                                 typename std::decay<__expr1>::type::filter_t,
                                 typename std::decay<__expr2>::type::filter_t>(
#ifdef LIBKET_L2R_EVALUATION
      expr2(expr0(expr))
#else
      expr0(expr2(expr))
#endif
    );
  }

  /// @brief Operator()
  /// @note  specialization for QExpression objects with two QGate and one QFilter expression
  template<std::size_t _qubits,
           QBackendType _qbackend,
           typename __expr0 = _expr0,
           typename __expr1 = _expr1,
           typename __expr2 = _expr2>
  inline constexpr auto operator()(QExpression<_qubits, _qbackend>& expr)
    const noexcept -> typename std::enable_if<
      std::is_base_of<QGate, typename std::decay<__expr0>::type>::value &&
        std::is_base_of<QGate, typename std::decay<__expr1>::type>::value &&
        std::is_base_of<filters::QFilter,
                        typename std::decay<__expr2>::type>::value,
      decltype(expr)>::type&
  {
    return _gate::template apply<_qubits,
                                 typename std::decay<__expr0>::type::filter_t,
                                 typename std::decay<__expr1>::type::filter_t,
                                 typename std::decay<__expr2>::type::filter_t>(
#ifdef LIBKET_L2R_EVALUATION
      expr1(expr0(expr))
#else
      expr0(expr1(expr))
#endif
    );
  }

  /// @brief Operator()
  /// @note  specialization for QExpression objects with three QGate expressions
  template<std::size_t _qubits,
           QBackendType _qbackend,
           typename __expr0 = _expr0,
           typename __expr1 = _expr1,
           typename __expr2 = _expr2>
  inline constexpr auto operator()(QExpression<_qubits, _qbackend>& expr)
    const noexcept -> typename std::enable_if<
      std::is_base_of<QGate, typename std::decay<__expr0>::type>::value &&
        std::is_base_of<QGate, typename std::decay<__expr1>::type>::value &&
        std::is_base_of<QGate, typename std::decay<__expr2>::type>::value,
      decltype(expr)>::type&
  {
    return _gate::template apply<_qubits,
                                 typename std::decay<__expr0>::type::filter_t,
                                 typename std::decay<__expr1>::type::filter_t,
                                 typename std::decay<__expr2>::type::filter_t>(
#ifdef LIBKET_L2R_EVALUATION
      expr2(expr1(expr0(expr)))
#else
      expr0(expr1(expr2(expr)))
#endif
    );
  }

  /// @brief Operator()
  /// @note  specialization for QFilterTag objects
  template<std::size_t _tag, typename __filter, typename __tagged_filter>
  inline constexpr auto operator()(
    const filters::QFilterTag<_tag, __filter, __tagged_filter>&) const noexcept
  {
    return TernaryQGate<_expr0,
                        _expr1,
                        _expr2,
                        _gate,
                        filters::QFilterTag<_tag, _filter, _filter>>(
      this->expr0, this->expr1, this->expr2);
  }

  /// @brief Operator()
  /// @note  specialization for QFilterGotoTag objects
  template<std::size_t _tag, typename __filter>
  inline constexpr auto operator()(
    const filters::QFilterGotoTag<_tag, __filter>&) const noexcept
  {
    return TernaryQGate<_expr0,
                        _expr1,
                        _expr2,
                        _gate,
                        filters::QFilterGotoTag<_tag, _filter>>(
      this->expr0, this->expr1, this->expr2);
  }

  /// @brief Operator()
  /// @note  specialization for generic expressions
  template<typename __expr>
  inline constexpr auto operator()(const __expr& expr) const noexcept
  {
    return TernaryQGate<decltype(typename std::decay<_expr0>::type{}(expr)),
                        typename std::decay<_expr1>::type,
                        typename std::decay<_expr2>::type,
                        _gate,
                        decltype(
                          typename filters::getFilter<_expr0>::type{}(
                            typename std::decay<__expr>::type::filter_t{})
                          << typename filters::getFilter<_expr1>::type{}
                          << typename filters::getFilter<_expr2>::type{})>(
      expr,
      typename std::decay<_expr1>::type{},
      typename std::decay<_expr2>::type{});
  }

  /// @brief Operator()
  /// @note  specialization for generic expressions
  template<typename __expr0, typename __expr1, typename __expr2>
  inline constexpr auto operator()(const __expr0& expr0,
                                   const __expr1& expr1,
                                   const __expr2& expr2) const noexcept
  {
    return TernaryQGate<
      decltype(typename std::decay<_expr0>::type{}(expr0)),
      decltype(typename std::decay<_expr1>::type{}(expr1)),
      decltype(typename std::decay<_expr2>::type{}(expr2)),
      _gate,
      decltype(typename filters::getFilter<_expr0>::type{}(
                 typename std::decay<__expr0>::type::filter_t{})
               << typename filters::getFilter<_expr1>::type{}(
                    typename std::decay<__expr1>::type::filter_t{})
               << typename filters::getFilter<_expr2>::type{}(
                    typename std::decay<__expr2>::type::filter_t{}))>(
      expr0, expr1, expr2);
  }

  /// @brief Prints the abstract syntax tree
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string
  template<std::size_t level = 1>
  std::string show() const noexcept
  {
    std::ostringstream os;
    using ::LibKet::show;
    show<level>(*this, os);
    return os.str();
  }

  /// @brief Prints the abstract syntax tree (DOT-language)
  ///
  /// @tparam level specifies the number of levels to print
  /// @result AST as a string (DOT-language)
  template<std::size_t level = 1>
  std::string dot() const noexcept
  {    
    std::ostringstream os;
    using ::LibKet::dot;
    dot<level>(*this, os);
    return os.str();
  }

  /// Returns adjoint gate
  ///
  /// @result expression representing the adjoint gate
  auto dagger() const noexcept
  {    
    using ::LibKet::dagger;
    return dagger(*this);
  }
};

/**
 @brief Serialize operation
*/
template<typename _expr0,
         typename _expr1,
         typename _expr2,
         typename _gate,
         typename _filter>
std::ostream&
operator<<(std::ostream& os,
           const TernaryQGate<_expr0, _expr1, _expr2, _gate, _filter>& gate)
{
  // Check if filter chain has changed from previous sub-expression
  if (std::is_same<
        typename std::decay<decltype(gate)>::type::filter_t,
        decltype(
          typename std::decay<decltype(gate)>::type::expr0_t::filter_t{}
          << typename std::decay<decltype(gate)>::type::expr1_t::filter_t{}
          << typename std::decay<decltype(gate)>::type::expr2_t::filter_t{})>::
      value) {
    // Output gate and next three sub-expressions
    os << typename std::decay<decltype(gate)>::type::gate_t()
       << gate.expr0 << "," << gate.expr1 << "," << gate.expr2 << ")";
  } else {
    // Output filter chain, gate and next three sub-expressions
    os << set
       << typename std::decay<decltype(gate)>::type::filter_t()
       << unset
       << typename std::decay<decltype(gate)>::type::gate_t()
       << gate.expr0 << "," << gate.expr1 << "," << gate.expr2 << "))";
  }  
  return os;
}

/**
   @brief Prints the abstract syntax tree of the expression

   @note  specialization for TernaryQGate objects
*/
template<std::size_t level = 1,
         typename _expr0,
         typename _expr1,
         typename _expr2,
         typename _gate,
         typename _filter>
inline static auto
show(const TernaryQGate<_expr0, _expr1, _expr2, _gate, _filter>& gate,
     std::ostream& os = std::cout,
     const std::string& prefix = "")
{
  os << "TernaryQGate\n";
  if (level > 0) {
    os << prefix << "|   gate = ";
    show<level - 1>(_gate{}, os, prefix + "|          ");
    os << prefix << "| filter = ";
    show<level - 1>(_filter{}, os, prefix + "|          ");
    os << prefix << "|  expr0 = ";
    show<level - 1>(gate.expr0, os, prefix + "|          ");
    os << prefix << "|  expr1 = ";
    show<level - 1>(gate.expr1, os, prefix + "|          ");
    os << prefix << "|  expr2 = ";
    show<level - 1>(gate.expr2, os, prefix + "|          ");
  }

  return gate;
}

/**
   @brief Prints the abstract syntax tree of the expression (DOT-language)

   @note  specialization for TernaryQGate objects
*/
template<std::size_t level = 1,
         typename _expr0,
         typename _expr1,
         typename _expr2,
         typename _gate,
         typename _filter>
inline static auto
dot(const TernaryQGate<_expr0, _expr1, _expr2, _gate, _filter>& gate,
    std::ostream& os = std::cout)
{
  os << "TernaryQGate\n";
  
  return gate;
}

/**
   @brief Creates the adjoint gate

   @note  specialization for TernaryQGate objects
*/
template<typename _expr0,
         typename _expr1,
         typename _expr2,
         typename _gate,
         typename _filter>
inline static auto
dagger(const TernaryQGate<_expr0, _expr1, _expr2, _gate, _filter>& gate)
{
  return TernaryQGate<
    decltype(dagger(_expr0{})),
    decltype(dagger(_expr1{})),
    decltype(dagger(_expr2{})),
    decltype(dagger(_gate{})),
    _filter>{};
}

/// @brief Type trait extracts the gate from an expression
template<
  typename _expr,
  bool = std::is_base_of<gates::QGate, typename std::decay<_expr>::type>::value>
struct getGate;

/// @brief Type trait extracts the gate from an expression
/// @note  specialization for UnaryQGate objects
template<typename _expr, typename _gate, typename _filter>
struct getGate<UnaryQGate<_expr, _gate, _filter>, true>
{
  using type = typename std::decay<_gate>::type;
};

/// @brief Type trait extracts the gate from an expression
/// @note  specialization for BinaryQGate objects
template<typename _expr0, typename _expr1, typename _gate, typename _filter>
struct getGate<BinaryQGate<_expr0, _expr1, _gate, _filter>, true>
{
  using type = typename std::decay<_gate>::type;
};

/// @brief Type trait extracts the gate from an expression
/// @note  specialization for TernaryQGate objects
template<typename _expr0,
         typename _expr1,
         typename _expr2,
         typename _gate,
         typename _filter>
struct getGate<TernaryQGate<_expr0, _expr1, _expr2, _gate, _filter>, true>
{
  using type = typename std::decay<_gate>::type;
};

/// @brief Type trait extracts the gate from an expression
/// @note  specialization for non-gate objects
template<typename _expr>
struct getGate<_expr, false>
{};

} // namespace gates

} // namespace LibKet

#endif // QGATE_HPP
