/** @file libket/gates/QGate_Init.hpp

    @brief C++ API quantum initialization gate class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup init Init gate
    @ingroup  unarygates
 */

#pragma once
#ifndef QGATE_INIT_HPP
#define QGATE_INIT_HPP

#include <QExpression.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief Init gate class

The Init gate class implements the
initialization gate for an arbitrary number of quantum bits

@ingroup init
*/
class QInit : public QGate
{
public:
  UNARY_GATE_DEFAULT_DECL(QInit, QInit);
  
  /// @{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    expr.append_kernel("BEGIN\n");
    expr.append_kernel("qubits " + utils::to_string(_qubits) + "\n");
    expr.append_kernel("cbits " + utils::to_string(_qubits) + "\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ////
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    expr.append_kernel("version 1.0\n");
    expr.append_kernel("qubits " + utils::to_string(_qubits) + "\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    expr.append_kernel("OPENQASM 2.0;\n");
    expr.append_kernel("include \"qelib1.inc\";\n");
    expr.append_kernel("qreg q[" + utils::to_string(_qubits) + "];\n");
    expr.append_kernel("creg c[" + utils::to_string(_qubits) + "];\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    expr.append_kernel("\tdef cphase,1,'R_{\\theta}'\n");
    expr.append_kernel("\tdef cphasedag,1,'R_{\\theta}'\n");
    expr.append_kernel("\tdef cy,1,'CY'\n");
    expr.append_kernel("\tdef cz,1,'CZ'\n");
    expr.append_kernel("\tdef i,0,'I'\n");
    expr.append_kernel("\tdef prep_x,0,'Prep_x'\n");
    expr.append_kernel("\tdef prep_y,0,'Prep_y'\n");
    expr.append_kernel("\tdef prep_z,0,'Prep_z'\n");
    expr.append_kernel("\tdef mx90,0,'mX_{90}'\n");
    expr.append_kernel("\tdef my90,0,'mY_{90}'\n");
    expr.append_kernel("\tdef reset,0,'Reset'\n");
    expr.append_kernel("\tdef rx,0,'R_x'\n");
    expr.append_kernel("\tdef rxdag,0,'R_x^\\dagger'\n");
    expr.append_kernel("\tdef ry,0,'R_y'\n");
    expr.append_kernel("\tdef rydag,0,'R_y^\\dagger'\n");
    expr.append_kernel("\tdef rz,0,'R_z'\n");
    expr.append_kernel("\tdef rzdag,0,'R_z^\\dagger'\n");
    expr.append_kernel("\tdef s,0,'S'\n");
    expr.append_kernel("\tdef sdag,0,'S^\\dagger'\n");
    expr.append_kernel("\tdef t,0,'T'\n");
    expr.append_kernel("\tdef tdag,0,'T^\\dagger'\n");
    expr.append_kernel("\tdef x,0,'X'\n");
    expr.append_kernel("\tdef x90,0,'X_{90}'\n");
    expr.append_kernel("\tdef y,0,'Y'\n");
    expr.append_kernel("\tdef y90,0,'Y_{90}'\n");
    expr.append_kernel("\tdef z,0,'Z'\n");

    for (std::size_t k = 0; k < 128; k++)
      expr.append_kernel("\tdef cphase" + utils::to_string(k) +
                         ",1,'R_{\\2 pi i/2^" + utils::to_string(k) + "}'\n");

    for (std::size_t i = 0; i < _qubits; ++i)
      expr.append_kernel("\tqubit q" + utils::to_string(i) + ";0\n");

    for (std::size_t i = 0; i < _qubits; ++i)
      expr.append_kernel("\tcbit c" + utils::to_string(i) + ";0\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    quest::initZeroState(expr.reg());
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    expr.append_kernel("DECLARE ro BIT[" + utils::to_string(_qubits) + "]\n");
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QULACS
  /// @brief Apply function
  /// @ingroup QULACS
  ///
  /// @note specialization for LibKet::QBackendType::Qulacs backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Qulacs>& apply(
    QExpression<_qubits, QBackendType::Qulacs>& expr) noexcept
  {
    expr.state().set_zero_state();
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    return expr;
  }
#endif
  /// @}
};

/**
   @brief Init gate creator
   @ingroup init

   This overload of the LibKet::gates::init() function can be used
   as the inner-most gate in a quantum expression
   
   \warning
   
   Be careful with using the initialization gate as terminal in
   quantum expressions that serve as sub-expressions. The following
   code creates two sub-expressions both using the initialization gate
   as terminal and creates them to another expression. The so-defined
   quantum expression is likely invalid since it re-initializes the
   quantum expr within the algorithm
   
   \code
   auto expr1 = gates::hadamard( gates::init() );
   auto expr2 = gates::hadamard( gates::init() );
   
   auto expr3 = expr1( expr2 );
   \endcode   
*/
inline constexpr auto
init() noexcept
{
  return UnaryQGate<filters::QFilter, QInit, filters::QFilterSelectAll>(
    filters::QFilter{});
}

// The QInit gate cannot use UNARY_GATE_OPTIMIZE_CREATOR_SINGLE macro
// to generated the optimized creator implementation since the
// terminal variant of the init() function creates the non-standard
// unary gate expression
//
// UnaryQGate
// |   gate = QInit
// | filter = QFilterSelectAll
// |   expr = QFilter
//
#ifdef LIBKET_OPTIMIZE_GATES
template<typename _expr, typename _filter>
inline constexpr auto
init(const UnaryQGate<_expr, QInit, _filter>& expr) noexcept
{ return expr; }

template<typename _expr, typename _filter>
inline constexpr auto
init(UnaryQGate<_expr, QInit, _filter>&& expr) noexcept
{ return expr; }
#endif // LIBKET_OPTIMIZE_GATES
  
/// @brief Init gate default implementation
/// @defgroup init_aliases Aliases
/// @ingroup init
/// @{
UNARY_GATE_DEFAULT_CREATOR(QInit, init);
GATE_ALIAS(init, INIT);
UNARY_GATE_DEFAULT_IMPL(QInit, init);
/// @}
  
} // namespace gates

} // namespace LibKet

#endif // QGATE_INIT_HPP
