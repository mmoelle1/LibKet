/** @file libket/gates/QGate_Rotate_Y.hpp

    @brief C++ API quantum Rotate_Y class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup rotate_y Rotate-Y gate
    @ingroup  unarygates
 */

#pragma once
#ifndef QGATE_ROTATE_Y_HPP
#define QGATE_ROTATE_Y_HPP

#include <QConst.hpp>
#include <QExpression.hpp>
#include <QFilter.hpp>
#include <QVar.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
template<typename _angle, typename _tol>
class QRotate_Ydag;

/**
@brief Rotate_Y gate class

The Rotate_Y gate class implements the quantum Rotate_Y gate
for an arbitrary number of quantum bits.

The Rotate_Y gate is a single-qubit rotation through angle
\f$\theta\f$ (radians) around the y-axis.

The unitary matrix reads

\f[
R_{y}(\theta) =
\begin{pmatrix}
\cos(\frac{\theta}{2}) & -\sin(\frac{\theta}{2})\\
\sin(\frac{\theta}{2}) & \cos(\frac{\theta}{2})
\end{pmatrix}
\f]

@ingroup rotate_y
*/
template<typename _angle, typename _tol = QConst_M_ZERO_t>
class QRotate_Y : public QGate
{
public:
  /// Rotation angle
  using angle = typename std::decay<_angle>::type;

  UNARY_GATE_DEFAULT_DECL_AT(QRotate_Y, QRotate_Ydag);

  /// @{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("RY[" + _angle::to_string() + "] q[" +
                           utils::to_string(i) + "]\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("cirq.YPowGate(exponent=" + _angle::to_string() +
                           ").on(q[" + utils::to_string(i) + "])\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      std::string _expr = "ry q[";
      for (auto i : _filter::range(expr))
        _expr += utils::to_string(i) +
                 (i != *(_filter::range(expr).end() - 1) ? "," : "], ");
      _expr += _angle::to_string() + "\n";
      expr.append_kernel(_expr);
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("ry(" + _angle::to_string(false) + ") q[" +
                           utils::to_string(i) + "];\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel([&]() { expr.kernel().ry(i, _angle::value()); });
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("\try q" + utils::to_string(i) + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        quest::rotateY(expr.reg(), i, (qreal)_angle::value());
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel("RY (" + _angle::to_string(false) + ") " +
                           utils::to_string(i) + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QULACS
  /// @brief Apply function
  /// @ingroup QULACS
  ///
  /// @note specialization for LibKet::QBackendType::Qulacs backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::Qulacs>& apply(
    QExpression<_qubits, QBackendType::Qulacs>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.circuit().add_RY_gate(i, (double)_angle::value());
    }
    return expr;
  }
#endif
  
#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(expr))
        expr.append_kernel(new qx::ry(i, _angle::value()));
    }
    return expr;
  }
#endif
  /// @}
};

/**
   @brief Rotate_Y gate creator
   @ingroup rotate_y

   This overload of the LibKet::gates::rotate_y() function can be used
   as terminal, i.e. the inner-most gate in a quantum expression
   
   \code
   auto expr = gates::rotate_y();
   \endcode
*/
template<typename _tol = QConst_M_ZERO_t, typename _angle>
inline constexpr auto rotate_y(_angle) noexcept
{
  return UnaryQGate<filters::QFilter, QRotate_Y<_angle, _tol>>(
    filters::QFilter{});
}

/// @brief Rotate_Y gate default implementation
/// @defgroup rotate_y_aliases Aliases
/// @ingroup rotate_y
/// @{
UNARY_GATE_OPTIMIZE_CREATOR_AT_NEGATIVE_IDENTITY(QRotate_Y, rotate_y);
UNARY_GATE_OPTIMIZE_CREATOR_AT_SAME_IDENTITY(QRotate_Ydag, rotate_y);
UNARY_GATE_OPTIMIZE_CREATOR_AT_ADD_SINGLE(QRotate_Y, rotate_y);
UNARY_GATE_OPTIMIZE_CREATOR_AT_SUB_SINGLE(QRotate_Ydag, rotate_y);
UNARY_GATE_DEFAULT_CREATOR_AT(QRotate_Y, rotate_y);
GATE_ALIAS_T(rotate_y, ROTATE_Y);
GATE_ALIAS_T(rotate_y, ry);
GATE_ALIAS_T(rotate_y, RY);
GATE_ALIAS_T(rotate_y, Ry);
/// @}

} // namespace gates

} // namespace LibKet

#endif // QGATE_ROTATE_Y_HPP
