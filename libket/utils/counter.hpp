/** @file libket/utils/counter.hpp

    @brief C++ API compile-time counter class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @ingroup utils

    @note This class is based on Fabio Alemagna's <personal@fabioalemagna.net>
    https://github.com/falemagn/fameta-counter
 */

#pragma once
#ifndef QUTILS_COUNTER_HPP
#define QUTILS_COUNTER_HPP

namespace LibKet {
namespace utils {

  struct default_counter_tag{};
  
  template <std::size_t StartN, std::size_t StartValue = 0, std::size_t Step = 1, typename Tag = default_counter_tag>
  class counter;

  template <std::size_t StartN, std::size_t StartValue, std::size_t Step, typename Tag>
  class counter {    
#if defined(__INTEL_COMPILER) || defined(_MSC_VER) || !defined(__cpp_decltype_auto)
    template <std::size_t N>
    struct slot {
#if defined(__INTEL_COMPILER)     
#   pragma warning push   
#   pragma warning disable 1624
#elif defined(__GNUC__) && !defined(__clang__)
#   pragma GCC diagnostic push
#   pragma GCC diagnostic ignored "-Wnon-template-friend"
#endif
      friend constexpr std::size_t slot_value(slot<N>);
#if defined(__INTEL_COMPILER)     
#   pragma warning pop  
#elif defined(__GNUC__) && !defined(__clang__)
#   pragma GCC diagnostic pop
#endif
    };
    
    template <std::size_t N, std::size_t I>
    struct writer {
      friend constexpr std::size_t slot_value(slot<N>) {
        return I;
      }
      
      static constexpr std::size_t value = I;
    };
    
    template <std::size_t N, std::size_t R = slot_value(slot<N>())>
    static constexpr std::size_t reader(std::size_t, slot<N>) {
      return R;
    };
#else
    template <std::size_t N>
    struct slot {
#if defined(__GNUC__) && !defined(__clang__)
#   pragma GCC diagnostic push
#   pragma GCC diagnostic ignored "-Wnon-template-friend"
#endif
      friend constexpr auto slot_value(slot<N>);
#if defined(__GNUC__) && !defined(__clang__)
#   pragma GCC diagnostic pop
#endif
    };
    
    template <std::size_t N, std::size_t I>
    struct writer {
      friend constexpr auto slot_value(slot<N>) {
        return I;
      }
      
      static constexpr std::size_t value = I;
    };
    
    template <std::size_t N, typename = decltype(slot_value(slot<N>()))>
    static constexpr std::size_t reader(std::size_t, slot<N>, std::size_t R = slot_value(slot<N>())) {
      return R;
    }
#endif
    
    static_assert(sizeof(writer<StartN, StartValue-Step>), "Base case");
    
    template <std::size_t N>
    static constexpr std::size_t reader(float, slot<N>, std::size_t R = reader(0, slot<N-1>())) {
      return R;
    }
    
  public:    
    
#if !defined(__clang_major__) || __clang_major__ > 7
    template <std::size_t N>
    static constexpr std::size_t next(std::size_t R = writer<N, reader(std::size_t(0), slot<N-1>())+Step>::value) {
      return R;
    }
    
    template <std::size_t N>
    static constexpr std::size_t current(std::size_t R = reader(std::size_t(0), slot<N-1>())) {
      return R;
    }    
#else
    template <std::size_t N>
    static constexpr std::size_t next(std::size_t R = writer<N, reader(std::size_t(0), slot<N>())+Step>::value) {
      return R;
    }
    
    template <std::size_t N>
    static constexpr std::size_t current(std::size_t R = reader(std::size_t(0), slot<N>())) {
      return R;
    }
#endif
  };
  
} // namespace utils
} // namespace LibKet

#endif // QUTILS_GRAPH_HPP
