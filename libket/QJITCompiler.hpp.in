/** @file libket/QJITCompiler.hpp

    @brief C++ API just-in-time compiler class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef QJITCOMPILER_HPP
#define QJITCOMPILER_HPP

#if defined(_WIN32)
#include <direct.h>
#include <windows.h>
#else
#include <dlfcn.h>
#include <sys/stat.h>
#endif

#include <limits>
#include <memory>

#include <QUtils.hpp>

#ifdef LIBKET_USE_PCH
#include <QJITCompiler.pch>
#endif

namespace LibKet {

  /**
     @Brief LibKet function pointer type declaration
   */
  typedef std::string(*QFunctionPtr_t)();

  template<QFunctionPtr_t _funcPtr>
  class QFunctionPtr
  {
  private:
    static const constexpr QFunctionPtr_t funcPtr = _funcPtr;
    
  public:
    static constexpr bool isExpr = false; // faking
    static constexpr std::size_t hash = 0;

    inline auto operator()() const noexcept
    {
      return funcPtr();
    }
  };
  
  /**
     @brief Just-in-time compiler configuration class
     
     The LibKet just-in-time compiler configuration class specifies the
     configuration of the just-in-time compiler
  */
  class QJITCompilerConfig
  {
  public:
    /// Default constructor
    QJITCompilerConfig()
      : cmd("missing"), flags("missing"), out("-o "), temp(utils::getLibKetPath())
    {
      char *env;
      env = std::getenv ("JIT_COMPILER_CMD");
      if(env!=NULL) cmd = env;
      
      env = std::getenv ("JIT_COMPILER_FLAGS");
      if(env!=NULL) flags = env;
      
      env = std::getenv ("JIT_COMPILER_TEMP");
      if(env!=NULL) temp = env;
    }
    
    virtual ~QJITCompilerConfig() { }
    
    /// Constructor from arguments
    QJITCompilerConfig(const std::string& cmd,
                       const std::string& flags,
                       const std::string& out,
                       const std::string& temp = utils::getLibKetPath())
      : cmd(cmd), flags(flags), out(out), temp(temp)
    {}
        
    /// Copy constructor
    QJITCompilerConfig(const QJITCompilerConfig& other)      
    {
      operator=(other);
    }

    /// Move constructor
    QJITCompilerConfig(QJITCompilerConfig&& other)
      : cmd(std::move(other.cmd)), flags(std::move(other.flags)),
        out(std::move(other.out)), temp(std::move(other.temp))
    {}
    
    /// Copy assignment operator
    QJITCompilerConfig& operator=(const QJITCompilerConfig& other)
    {
      cmd   = other.cmd;
      flags = other.flags;
      out   = other.out;
      temp  = other.temp;
      return *this;
    }
    
    /// Move assignment operator
    QJITCompilerConfig& operator=(QJITCompilerConfig&& other)
    {
      cmd   = std::move(other.cmd);
      flags = std::move(other.flags);
      out   = std::move(other.out);
      temp  = std::move(other.temp);
      return *this;
    }
    
    /// Returns compiler command
    virtual const std::string& getCmd() const { return cmd; }

    /// Returns compiler flags
    virtual const std::string& getFlags() const { return flags; }

    /// Returns compiler output flag
    virtual const std::string& getOut() const { return out; }
    
    /// Returns compiler temporal directory
    virtual const std::string& getTemp() const { return temp; }

    /// Sets compiler command
    void setCmd(const std::string& _cmd)
    { this->cmd = _cmd; }

    /// Sets compiler flags
    void setFlags(const std::string& _flags)
    { this->flags = _flags; }

    /// Sets compiler output flag
    void setOut(const std::string& _out)
    { this->out = _out; }
    
    /// Sets compiler temporal directory
    void setTemp(const std::string& _temp)
    { this->temp = _temp; }

    /// Prints the object as a string
    std::ostream& print(std::ostream &os) const
    {
        os << "JIT Compiler.\n"
           << "  cmd:                " << cmd << "\n"
           << "  flags:              " << flags << "\n"
           << "  output flag:        " << out << "\n"
           << "  temporal directory: " << temp << "\n";
        
        return os;
    }

    /// Initializes to native compiler
    static QJITCompilerConfig native()
    {
#if defined(_WIN32)
      return QJITCompilerConfig("@JIT_CXX_COMPILER@",
                                "@JIT_CXX_FLAGS@ @JIT_INCLUDE_DIRECTORIES_WIN32@ @JIT_LIBRARIES_WIN32@",
                                "/Fo"); //no space
#else
      return QJITCompilerConfig("@JIT_CXX_COMPILER@",
                                "@JIT_CXX_FLAGS@ @JIT_INCLUDE_DIRECTORIES@ @JIT_LIBRARIES@",
                                "-o ");
#endif
    }
    
    /// Initializes to default Clang compiler
    static QJITCompilerConfig clang()
    {
      return QJITCompilerConfig("clang++",
                                "-O3 -shared -std=c++14 @JIT_INCLUDE_DIRECTORIES@ @JIT_LIBRARIES@",
                                "-o ");
    }
    
    /// Initializes to default GCC compiler
    static QJITCompilerConfig gcc()
    {      
      return QJITCompilerConfig("g++",
                                "-fPIC -O3 -shared -std=c++14 @JIT_INCLUDE_DIRECTORIES@ @JIT_LIBRARIES@",
                                "-o ");
    }
    
    /// Initializes to default Intel compiler
    static QJITCompilerConfig intel()
    {
#if defined(_WIN32)
      return QJITCompilerConfig("icl",
                                "/O3 /dll /Qstd=c++14 @JIT_INCLUDE_DIRECTORIES_WIN32@ @JIT_LIBRARIES@_WIN32",
                                "-o ");
#else
      return QJITCompilerConfig("icpc",
                                "-O3 -shared -std=c++14 @JIT_INCLUDE_DIRECTORIES@ @JIT_LIBRARIES@",
                                "/Fo"); //no space
#endif
    }
    
    /// Initializes to default Microsoft Visual Studio compiler
    static QJITCompilerConfig msvc()
    {
      return QJITCompilerConfig("cl.exe",
                                "/EHsc /Ox /LD /std:c++14 @JIT_INCLUDE_DIRECTORIES_WIN32@ @JIT_LIBRARIES_WIN32@",
                                "/Fo"); //no space
    }
    
    /// Initializes to default PGI compiler
    static QJITCompilerConfig pgi()
    {      
      return QJITCompilerConfig("pgc++",
                                "-O3 -shared -std=c++14 @JIT_INCLUDE_DIRECTORIES@ @JIT_LIBRARIES@",
                                "-o ");
    }
    
    /// Initializes to default Oracle/SunStudio compiler
    static QJITCompilerConfig sunstudio()
    {
      return QJITCompilerConfig("sunstudio",
                                "-O3 -shared -std=c++14 @JIT_INCLUDE_DIRECTORIES@ @JIT_LIBRARIES@",
                                "-o ");
    }
    
    /// Tries to initialize compiler automatically based on the context
    static QJITCompilerConfig guess()
    {
#     if defined(__INTEL_COMPILER)
      return intel();
        
#     elif  defined(_MSC_VER)
      return msvc();
      
#     elif defined(__clang__)
      return clang();
        
#     elif defined(__GNUC__)
      return gcc();
      
#     elif defined(__PGIC__)
      return pgi();
        
#     elif defined(__SUNPRO_CC)
      return sunstudio();
        
#     endif
    }
    
  protected:
    /// Member variables
    std::string cmd;
    std::string flags;
    std::string out;
    std::string temp;

  };
  
  /// Serialize operator
  std::ostream &operator<<(std::ostream &os, const QJITCompilerConfig& config)
  {
    return config.print(os);
  }

/**
   @brief Dynamic library class
   
   The LibKet dynamic library class stores a pointer to a dynamic
   library that can be compiled and linked at runtime.
 */
  class QDynamicLibrary
  {
  public:
    
    /// Default constructor deleted
    QDynamicLibrary() = delete;
      
    /// Constructor from file
    QDynamicLibrary(const char* filename, int flag)
    {
      QDebug << "Loading dynamic library: " << filename << "\n";
      
#if defined(_WIN32)
      static_cast<void>(flag);
      HMODULE dl = LoadLibrary(filename);
      if (!dl)
        {
          std::ostringstream err;
          err <<"LoadLibrary - error: " << GetLastError();
          LIBKET_ERROR( err.str() );
        }
      handle.reset(dl, FreeLibrary);
#elif defined(__APPLE__) || defined(__linux__) || defined(__unix)        
      void * dl = ::dlopen(filename, flag);
      if (!dl)
        LIBKET_ERROR( ::dlerror() );
      handle.reset(dl, ::dlclose);
#else
#error("Unsupported operating system")
#endif
    }

    /// Destructor
    ~QDynamicLibrary()
    {
      //if (handle)
      //handle.reset(dl, ::dlclose);
    }
    
    /// Gets symbol from dynamic library
    template<class T>
    T* getSymbol(const char* name) const
    {
      if (!handle)
        LIBKET_ERROR("An error occured while accessing the dynamic library");
      
      T *symbol = NULL;
#if defined(_WIN32)
      *(void **)(&symbol) = (void*)GetProcAddress(handle.get(), name );
#elif defined(__APPLE__) || defined(__linux__) || defined(__unix)
      *(void **)(&symbol) = ::dlsym( handle.get(), name );
#endif
      if (!symbol)
        LIBKET_ERROR("An error occured while getting symbol from the dynamic library");
      
      return symbol;
    }
    
    /// Checks if handle is assigned
    operator bool() const { return (bool)handle; }
    
  private:
    
    /// Handle to dynamic library object
#if defined(_WIN32)
    std::shared_ptr< std::remove_pointer<HMODULE>::type > handle;
#else //if defined(__APPLE__) || defined(__linux__) || defined(__unix)
    std::shared_ptr<void> handle;
#endif
  };

/**
   @brief Just-in-time compiler class
     
   The LibKet just-in-time compiler class compiles source code at
   runtime and links it to the running binary. This mechanism makes it
   possible to generate source code based on user-defined run-time
   parameters and still perform compile-time optimization.
*/
  class QJITCompiler
  {    
  public:
    
    /// Default constructor
    QJITCompiler()
      : kernel(), config()
    {}
    
    /// Copy constructor
    QJITCompiler(const QJITCompiler& other)
      : config(other.config)
    {
      kernel << other.kernel.rdbuf();
    }

    /// Move constructor
    QJITCompiler(QJITCompiler&& other)
      : config(std::move(other.config))
    {
      kernel << other.kernel.rdbuf();
    }
    
    /// Constructor from compiler configuration
    explicit QJITCompiler(const QJITCompilerConfig& config)
      : kernel(), config(config)
    {}
    
    /// Copy assignment operator
    QJITCompiler& operator=(const QJITCompiler& other)
    {
      kernel << other.kernel.rdbuf();
      config = other.config;
      return *this;
    }
    
    /// Move assignment operator
    QJITCompiler& operator=(QJITCompiler&& other)
    {
      kernel << other.kernel.rdbuf();
      config = std::move(other.config);
      return *this;
    }
    
    /// Inputs kernel source code from string
    QJITCompiler & operator<<(const std::string& source)
    {
      kernel << source;
      return *this;
    }
    
    /// Inputs kernel source code from input stream
    QJITCompiler & operator<<(std::istream & source)
    {
      kernel << source.rdbuf();
      return *this;
    }
    
    /// Compiles kernel source code into dynamic library determining
    /// the filename from hash of kernel source code automatically
    QDynamicLibrary build(bool force = false)
    {
      size_t h = std::hash<std::string>()(getKernel().str()+config.getCmd()+config.getFlags());
      return build(std::to_string(h), force);
    }
    
    /// Compiles kernel source code into dynamic library using given
    /// filename
    QDynamicLibrary build(const std::string &name, bool force = false)
    {
      // Prepare library name
      std::stringstream libName;
      
#     if   defined(_WIN32)
      libName << config.getTemp() << "lib" << name << ".dll";
      //(void)std::system("del /f " + libName);
      //force = true;
#     elif defined(__APPLE__)
      libName << config.getTemp() << "lib" << name << ".dylib";
#     elif defined(unix) || defined(__unix__) || defined(__unix)
      libName << config.getTemp() << "lib" << name << ".so";
#     else
#     error("Unsupported operating system")
#     endif
      
      // Compile library (if required)
      std::ifstream libfile(libName.str().c_str());
      if(!libfile || force)
        {
          // Check if output directory exists
          switch (utils::dirExists(config.getTemp().c_str())) {
          case (0) : // does not exist, create
#           if defined(_WIN32)
            if (_mkdir(config.getTemp().c_str()) != 0)
              LIBKET_ERROR("An error occured while creating the output directory");
#           else
            if (mkdir(config.getTemp().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == -1)
              LIBKET_ERROR("An error occured while creating the output directory");
#           endif
            break;
          case (2) : // exists as a file
            LIBKET_ERROR("File with the same name exists");
            break;
          }

          // Write kernel source code to file
          std::stringstream srcName;
#         ifdef _WIN32          
          srcName<< config.getTemp() << name << ".cxx";
          std::ofstream file(srcName.str().c_str());
          file << "#ifdef __cplusplus\n";
          file << "#define EXPORT extern \"C\" __declspec(dllexport)\n";
          file << "#endif\n";
#         else
          srcName<< config.getTemp() << name << ".cxx";
          std::ofstream file(srcName.str().c_str());
          file << "#ifdef __cplusplus\n";
          file << "#define EXPORT extern \"C\"\n";
          file << "#endif\n";
#         endif
          file << getKernel().str() <<"\n";
          file.close();
          
          // Compile kernel source code into library
          std::stringstream systemcall;
          
#         ifdef _WIN32
          // double quotes are better than single quotes..
          systemcall << "\"\"" << config.getCmd() << "\" "
                     << config.getFlags() << " \""
                     << srcName.str()     << "\" "
                     << config.getOut() << "\"" << libName.str() << "\"\"";
#         else
          systemcall << "\"" << config.getCmd() << "\" "
                     << config.getFlags() << " \""
                     << srcName.str()     << "\" "
                     << config.getOut() << "\"" << libName.str() << "\"";
#         endif
          
          QDebug << "Compiling dynamic library: " << systemcall.str() << "\n";
          if(std::system(systemcall.str().c_str()) != 0)
            LIBKET_ERROR("An error occured while compiling the kernel source code");
        }
      
#ifdef _WIN32
      return QDynamicLibrary( libName.str().c_str(), 0 );
#else
      return QDynamicLibrary( libName.str().c_str(), RTLD_LAZY
#ifdef RTLD_DEEPBIND
                              | RTLD_DEEPBIND
#endif
                              );
#endif
    }
    
    /// Clears kernel source code
    void clear()
    {
      kernel.clear();
      kernel.str(std::string());
    }
    
    /// Prints the object as a string
    std::ostream& print(std::ostream &os) const
    {
      os << kernel.str();
      return os;
    }
    
    /// Returns kernel source code (as output stringstream)
    const std::ostringstream &getKernel() const
    {
      return kernel;
    }
    
    /// Returns pointer to kernel source code
    std::ostringstream &getKernel()
    {
      return kernel;
    }

    /// Returns constant reference to configuration
    const QJITCompilerConfig &getConfig() const
    {
      return config;
    }
    
  private:
    /// Kernel source code
    std::ostringstream kernel;
    
    /// Compiler configuration
    QJITCompilerConfig config;
  };
  
  /// Serialize operator
  std::ostream &operator<<(std::ostream &os, const QJITCompiler& compiler)
  {
    return compiler.print(os);
  }
    
  /// Generates callable quantum expression object from string
  /// representation of a quantum expression just-in-time
  template<std::size_t _qubits, QBackendType _qbackend>
  QExpression<_qubits, _qbackend>& gen_expression(const std::string& expression, QExpression<_qubits, _qbackend>& expr)
  {    
    QJITCompiler compiler(QJITCompilerConfig::native());
    std::string fname = "expr" + std::to_string(utils::hash((expression+
                                                             std::to_string(_qubits)+
                                                             QBackendTypeMap[_qbackend]+
                                                             compiler.getConfig().getCmd()+
                                                             compiler.getConfig().getFlags()).c_str()
                                                            ));
    
    compiler << "// *** AUTO-GENERATED QUANTUM EXPRESSION ***\n"
             << "#include <LibKet_JIT.hpp>\n"
             << "\n"
             << "using namespace LibKet;\n"
             << "using namespace LibKet::circuits;\n"
             << "using namespace LibKet::filters;\n"
             << "using namespace LibKet::gates;\n"
             << "\n"
             << "namespace LibKet {\n"
             << "EXPORT QExpression<"
             << std::to_string(_qubits)
             << ","
             << QBackendTypeMap[_qbackend]
             << ">& "
             << fname << "(QExpression<"
             << std::to_string(_qubits)
             << ","
             << QBackendTypeMap[_qbackend]
             << ">& expr)\n"
             << "\t{\n"
             << "\tauto e = " << expression << ";\n"
             << "\treturn e(expr);\n"
             << "\t}\n"
             << "};\n"           
             << "// *** AUTO-GENERATED QUANTUM EXPRESSION ***\n";
    
    QDebug << compiler;

    try {
      QDynamicLibrary dl = compiler.build(std::to_string(utils::hash((expression+
                                                                      std::to_string(_qubits)+
                                                                      QBackendTypeMap[_qbackend]+
                                                                      compiler.getConfig().getCmd()+
                                                                      compiler.getConfig().getFlags()).c_str())));
      try {
        typedef QExpression<_qubits, _qbackend>& (dl_expr)(QExpression<_qubits, _qbackend>&);
        dl_expr * e = dl.getSymbol<dl_expr>(fname.c_str());
        
        try {
          return e(expr);
        } catch(...) {
          LIBKET_ERROR("An error occured while executing the quantum expression");
          return expr;
        }
      } catch (...) {
        LIBKET_ERROR("An error occured while loading the dynamic library");
        return expr;
      }      
    } catch(...) {
      LIBKET_ERROR("An error occured while compiling the quantum expression");
      return expr;
    }
  }
  
} // namespace LibKet

#endif // QJITCOMPILER_HPP
