/** @file libket/devices/QDevice_QX.hpp

    @brief C++ API QX-simulator device class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QDEVICE_QX_HPP
#define QDEVICE_QX_HPP

#include <string>

#include <QArray.hpp>
#include <QBase.hpp>
#include <QDevice.hpp>
#include <QUtils.hpp>

namespace LibKet {

#ifdef LIBKET_WITH_QX
/**
   @brief QX-simulator device class

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_QX : public QExpression<_qubits, QBackendType::QX>
{
private:
  /// Error probability
  const double error_probability;

  /// Verbosity level
  const bool verbose;

  /// Silence level
  const bool silent;

  /// Binary mode
  const bool only_binary;

  /// Number of shots
  const std::size_t shots;

  /// Base type
  using Base = QExpression<_qubits, QBackendType::QX>;

public:
  /// Constructors from base class
  using Base::Base;

  /// Constructor from parameter list
  QDevice_QX(
    double error_probability = std::atof(LibKet::getenv("QX_ERROR_PROBABILITY",
                                                        "0.0")),
    bool verbose = (bool)std::atoi(LibKet::getenv("QX_VERBOSE", "0")),
    bool silent = (bool)std::atoi(LibKet::getenv("QX_SILENT", "0")),
    bool only_binary = (bool)std::atoi(LibKet::getenv("QX_ONLY_BINARY", "0")),
    std::size_t shots = std::atoi(LibKet::getenv("QX_SHOTS", "1024")))
    : error_probability(error_probability)
    , verbose(verbose)
    , silent(silent)
    , only_binary(only_binary)
    , shots(shots)
  {}

  /// Constructor from JSON object
  QDevice_QX(const utils::json& config)
    : QDevice_QX(config.find("error_probability") != config.end()
                   ? config["error_probability"].get<double>()
                   : std::atof(LibKet::getenv("QX_ERROR_PROBABILITY", "0.0")),
                 config.find("verbose") != config.end()
                   ? config["verbose"].get<bool>()
                   : (bool)std::atoi(LibKet::getenv("QX_VERBOSE", "0")),
                 config.find("silent") != config.end()
                   ? config["silent"].get<bool>()
                   : (bool)std::atoi(LibKet::getenv("QX_SILENT", "0")),
                 config.find("only_binary") != config.end()
                   ? config["only_binary"].get<bool>()
                   : (bool)std::atoi(LibKet::getenv("QX_ONLY_BINARY", "0")),
                 config.find("shots") != config.end()
                   ? config["shots"].get<size_t>()
                   : std::atoi(LibKet::getenv("QX_SHOTS", "1024")))
  {}

  /// Apply expression to base type
  template<typename Expr>
  QDevice_QX& operator()(const Expr& expr)
  {
    expr(*reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_QX& operator()(const std::string& expr)
  {
    gen_expression(expr, *reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Execute quantum circuit locally on QX-simulator asynchronously
  /// and return pointer to job
  QJob<QJobType::CXX>* execute_async(
    std::size_t shots = 0,
    std::function<void(QDevice_QX*, qx::circuit&, qx::qu_register&)> func_init =
      NULL,
    std::function<void(QDevice_QX*, qx::circuit&, qx::qu_register&)>
      func_before = NULL,
    std::function<void(QDevice_QX*, qx::circuit&, qx::qu_register&)>
      func_after = NULL,
    QStream<QJobType::CXX>* stream = NULL)
  {
    if (std::abs(error_probability) > 0.0) {
      auto runner = [&, shots]() -> void {
        if (func_init)
          func_init(this, this->circuit(), this->reg());
        qx::depolarizing_channel _dep_ch(
          &(this->circuit()), _qubits, this->error_probability);
        qx::circuit* _circuit = _dep_ch.inject(this->verbose);
        _circuit->set_iterations(shots > 0 ? shots : this->shots);
        if (func_before)
          func_before(this, *_circuit, this->reg());
        _circuit->execute(
          this->reg(), this->verbose, this->silent, this->only_binary);
        if (func_after)
          func_after(this, *_circuit, this->reg());
      };

      if (stream != NULL)
        return stream->run(runner);
      else
        return _qstream_cxx.run(runner);
    } else {
      auto runner = [&, shots]() -> void {
        if (func_init)
          func_init(this, this->circuit(), this->reg());
        this->circuit().set_iterations(shots > 0 ? shots : this->shots);
        if (func_before)
          func_before(this, this->circuit(), this->reg());
        this->circuit().execute(
          this->reg(), this->verbose, this->silent, this->only_binary);
        if (func_after)
          func_after(this, this->circuit(), this->reg());
      };

      if (stream != NULL)
        return stream->run(runner);
      else
        return _qstream_cxx.run(runner);
    }
  }

  /// Execute quantum circuit locally on QX-simulator synchronously
  /// and return pointer to job
  QJob<QJobType::CXX>* execute(
    std::size_t shots = 0,
    std::function<void(QDevice_QX*, qx::circuit&, qx::qu_register&)> func_init =
      NULL,
    std::function<void(QDevice_QX*, qx::circuit&, qx::qu_register&)>
      func_before = NULL,
    std::function<void(QDevice_QX*, qx::circuit&, qx::qu_register&)>
      func_after = NULL,
    QStream<QJobType::CXX>* stream = NULL)
  {
    return execute_async(shots, func_init, func_before, func_after, stream)
      ->wait();
  }

  /// Execute quantum circuit locally on QX-simulator synchronously
  /// and return result
  qx::qu_register& eval(
    std::size_t shots = 0,
    std::function<void(QDevice_QX*, qx::circuit&, qx::qu_register&)> func_init =
      NULL,
    std::function<void(QDevice_QX*, qx::circuit&, qx::qu_register&)>
      func_before = NULL,
    std::function<void(QDevice_QX*, qx::circuit&, qx::qu_register&)>
      func_after = NULL,
    QStream<QJobType::CXX>* stream = NULL)
  {
    execute_async(shots, func_init, func_before, func_after, stream)->wait();
    return this->reg();
  }

public:
  /// Get state with highest probability from internal data object
  template<QResultType _type>
  static auto get() ->
    typename std::enable_if<_type == QResultType::best, std::size_t>::type
  {
    assert(get<QResultType::status>());
    return 0;
  }

  /// Get duration from internal data object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get() ->
    typename std::enable_if<_type == QResultType::duration,
                            std::chrono::duration<Rep, Period>>::type
  {
    assert(get<QResultType::status>());
    return std::chrono::duration<Rep, Period>(0);
  }

  /// Get histogram from internal data object
  template<QResultType _type, class T = std::size_t>
  static auto get() ->
    typename std::enable_if<_type == QResultType::histogram,
                            QArray<(1 << _qubits), T, QEndianness::lsb>>::type
  {
    assert(get<QResultType::status>());
    QArray<(1 << _qubits), T, QEndianness::lsb> _histogram;

    return _histogram;
  }

  /// Get unique identifier from internal data object
  template<QResultType _type>
  static auto get() ->
    typename std::enable_if<_type == QResultType::id, std::string>::type
  {
    assert(get<QResultType::status>());
    return std::string("0");
  }

  /// Get success status from internal data object
  template<QResultType _type>
  static auto get() ->
    typename std::enable_if<_type == QResultType::status, bool>::type
  {
    return true;
  }

  /// Get time stamp from internal data object
  template<QResultType _type>
  static auto get() ->
    typename std::enable_if<_type == QResultType::timestamp, std::time_t>::type
  {
    assert(get<QResultType::status>());
    return std::time(nullptr);
  }
};

#else

/**
   @brief QX-simulator device class

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_QX : public QDevice_Dummy
{
  using QDevice_Dummy::QDevice_Dummy;
};

#endif

#define QDeviceDefineQX(_type)                                                 \
  template<std::size_t _qubits>                                                \
  class QDevice<_type,                                                         \
                _qubits,                                                       \
                device::QDeviceProperty<_type>::simulator,                     \
                device::QDeviceProperty<_type>::endianness>                    \
    : public QDevice_QX<_qubits>                                               \
  {                                                                            \
  public:                                                                      \
    QDevice(double error_probability =                                         \
              std::atof(LibKet::getenv("QX_ERROR_PROBABILITY", "0.0")),        \
            bool verbose = (bool)std::atoi(LibKet::getenv("QX_VERBOSE", "0")), \
            bool silent = (bool)std::atoi(LibKet::getenv("QX_SILENT", "0")),   \
            bool only_binary = (bool)                                          \
              std::atoi(LibKet::getenv("QX_ONLY_BINARY", "0")),                \
            std::size_t shots = std::atoi(LibKet::getenv("QX_SHOTS", "1024"))) \
      : QDevice_QX<_qubits>(error_probability,                                 \
                            verbose,                                           \
                            silent,                                            \
                            only_binary,                                       \
                            shots)                                             \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
                                                                               \
    QDevice(const utils::json& config)                                         \
      : QDevice(config.find("error_probability") != config.end()               \
                  ? config["error_probability"].get<double>()                  \
                  : std::atof(LibKet::getenv("QX_ERROR_PROBABILITY", "0.0")),  \
                config.find("verbose") != config.end()                         \
                  ? config["verbose"].get<bool>()                              \
                  : (bool)std::atoi(LibKet::getenv("QX_VERBOSE", "0")),        \
                config.find("silent") != config.end()                          \
                  ? config["silent"].get<bool>()                               \
                  : (bool)std::atoi(LibKet::getenv("QX_SILENT", "0")),         \
                config.find("only_binary") != config.end()                     \
                  ? config["only_binary"].get<bool>()                          \
                  : (bool)std::atoi(LibKet::getenv("QX_ONLY_BINARY", "0")),    \
                config.find("shots") != config.end()                           \
                  ? config["shots"].get<size_t>()                              \
                  : std::atoi(LibKet::getenv("QX_SHOTS", "1024")))             \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
  };

namespace device {

QDevicePropertyDefine(QDeviceType::qx, "QX", 26, true, QEndianness::lsb);

} // namespace device

QDeviceDefineQX(QDeviceType::qx);

} // namespace LibKet

#endif // QDEVICE_QX_HPP
