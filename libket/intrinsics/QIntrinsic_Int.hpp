/** @file libket/intrinsics/QIntrinsic_Int.hpp

    @brief C++ API quantum intrinsic integer data type class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @ingroup intrinsics
 */

#pragma once
#ifndef QINTRINSIC_INT_HPP
#define QINTRINSIC_INT_HPP

#include <QArray.hpp>

#include <intrinsics/QIntrinsic.hpp>

namespace LibKet {

namespace intrinsics {

/**
   @brief Adder quantum algorithms

   The LibKet adder quantum algorithm enumerator defines the supported
   adder quantum algorithms
*/
enum class QAddType
{

  /**
     @brief Carry lookahead adder

     This adder is described in the document

     A Logarithmic-Depth Quantum Carry-Lookahead Adder

     (https://arxiv.org/pdf/quant-ph/0406142) by Thomas G. Draper,
     Samuel A. Kutin, Eric M. Rains, and Krysta M. Svore
   */
  carry_lookahead,
  carry_lookahead_ctrl,

  /**
     @brief Draper adder

     This adder is described in the document

     Addition on a Quantum Computer

     (https://arxiv.org/abs/quant-ph/0008033) by Thomas G. Draper
   */
  qft,
  qft_ctrl,

  /**
     @brief Ripple carry adder

     This adder is described in the document

     A new quantum ripple-carry addition circuit

     (https://arxiv.org/abs/quant-ph/0410184) by Steven A. Cuccaro,
     Thomas G. Draper, Samuel A. Kutin, and David Petrie Moulton
   */
  ripple_carry,
  ripple_carry_ctrl,

  /**
     @brief T-count optimized adder

     This adder is described in the document

     T-count Optimized Design of Quantum Integer Multiplication

     (https://arxiv.org/abs/1706.05113) by Edgard Muñoz-Coreas, and
     Himanshu Thapliyal
   */
  t_count,
  t_count_ctrl,
};

/**
   @brief Multiplier quantum algorithms

   The LibKet multiplier quantum algorithm enumerator defines the
   supported multiplier quantum algorithms
*/
enum class QMulType
{
  /**
     @brief Karatsuba-based multiplier

     This multiplier is described in the document

     Improved reversible and quantum circuits for Karatsuba-based
     integer multiplication

     (https://arxiv.org/abs/1706.03419) by Alex Parent, Martin
     Roetteler, and Michele Mosca
   */
  Karatsuba1,
  Karatsuba1_ctrl,

  /**
     @brief Karatsuba-based multiplier

     This multiplier is described in the document

     Asymptotically Efficient Quantum Karatsuba Multiplication

     (https://arxiv.org/abs/1904.07356) by Craig Gidney
  */
  Karatsuba2,
  Karatsuba2_ctrl
};

/**
 @brief Integer division quantum algorithms

 The LibKet integer division quantum algorithm enumerator defines
 the supported integer division quantum algorithms
*/
enum class QDivType
{
  /**
     @brief T-count optimized division algorithm

     This division algorithm is described in the document

     Quantum Circuit Design of Integer Division Optimizing Ancillary
     Qubits and T-Count

     (https://arxiv.org/abs/1609.01241) by Himanshu Thapliyal,
     T. S. S. Varun, and Edgard Munoz-Coreas
   */
  t_count,
  t_count_ctrl
};

/**
@brief Quantum intrinsic integer data type class

The Quantum intrinsic integer data type class implements integer data
types
*/
template<typename bits>
class QInt : public QIntrinsic
{};

} // namespace intrinsics

} // namespace LibKet

#endif // QINTRINSIC_INT_HPP
