/** @file libket/QProgram.hpp

    @brief C++ API quantum program class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @ingroup cxx_api
 */

#pragma once
#ifndef QPROGRAM_HPP
#define QPROGRAM_HPP

#include <QExpression.hpp>
#include <QFilter.hpp>
#include <QGates.hpp>

/// @brief Macro declares QProgram member functions for a unary LibKet
/// function with no arguments, e.g.
///
/// \code{.cpp}
///   QProgram prog;
///   prog.h( {1,2} );
/// \endcode
///
/// applies a Hadamard gate to qubits 1 and 2.
#define PROGRAM_ADD_UNARY(_prog_func, _func)                       \
  void _prog_func(std::size_t qubit)                                    \
  {                                                                     \
    _prog_func({qubit});                                                \
  }                                                                     \
                                                                        \
  void _func(const std::initializer_list<std::size_t>& qubits)          \
  {                                                                     \
    std::string expr = #_func"(sel_<";                                  \
    for (auto it=qubits.begin(); it!=qubits.end(); ++it)                \
      expr += std::to_string(*it) + (it!=qubits.end()-1 ? "," : "");    \
    expr += ">(" + _expr + "))";                                        \
    _expr = expr;                                                       \
  }                                                                     \
                                                                        \
  template<typename T,                                                  \
           typename = typename std::enable_if<std::is_base_of<QFilter,  \
                                                              typename std::decay<T>::type>::value, \
                                              void>::type>              \
  void _prog_func(const T& filter)                                      \
  {                                                                     \
    std::stringstream ss;                                               \
    ss << set << filter << unset;                                       \
    _expr = #_func"(" + ss.str() + _expr + "))";                        \
  }

/// @brief Macro declares QProgram member functions for a unary LibKet
/// function with one argument and one (optional) tolerance parameter
/// that is passed as template argument, e.g.
///
/// \code{.cpp}
///   QProgram prog;
///   prog.rx(3.141, {1,2} );
///   prog.rx(3.141, {1,2}, 0.01 );
/// \endcode
///
/// applies a rotation about the x-axis to qubits 1 and 2 under the
/// condition that the magnitude of the rotation angle is larger than
/// or equal to 0.01
#define PROGRAM_ADD_UNARY_ANGLE_TOL(_prog_func, _func)                  \
  void _prog_func(double angle, std::size_t qubit, double tol)          \
  {                                                                     \
    _prog_func(angle, {qubit}, tol);                                    \
  }                                                                     \
                                                                        \
  void _prog_func(double angle,                                         \
                  const std::initializer_list<std::size_t>& qubits,     \
                  double tol = 0.0)                                     \
  {                                                                     \
    std::string expr = #_func"(" + genQConst(angle) + ",sel_<";         \
    for (auto it=qubits.begin(); it!=qubits.end(); ++it)                \
      expr += std::to_string(*it) + (it!=qubits.end()-1 ? "," : "");    \
    expr += ">(" + _expr + "))";                                   \
    _expr = expr;                                                       \
  }                                                                     \
                                                                        \
  template<typename T,                                                  \
           typename = typename std::enable_if<std::is_base_of<QFilter,  \
                                                              typename std::decay<T>::type>::value, \
                                              void>::type>              \
  void _prog_func(double angle, const T& filter, double tol = 0.0)      \
  {                                                                     \
    std::stringstream ss;                                               \
    ss << set << filter << unset;                                       \
    _expr = #_func"<" + genQConst_t(tol) + ">(" + genQConst(angle) + "," + ss.str() + _expr + "))"; \
  }

/// @brief Macro declares QProgram member functions for a unary LibKet
/// function with no argument and arbitrary number of template
/// parameters, e.g.
///
/// \code{.cpp}
///   QFunctor_alias(ftor, measure(h()) );
///   QProgram prog;
///   prog.hook<ftor>( {1,2} );
/// \endcode
///
/// applies Hadamard gates to qubits 1 and 2 and performs measurement
/// afterward.
#define PROGRAM_ADD_UNARY_TPL(_prog_func, _func)              \
  void _prog_func(double angle, std::size_t qubit, double tol)          \
  {                                                                     \
    _prog_func(angle, {qubit}, tol);                                    \
  }                                                                     \
                                                                        \
  void _prog_func(double angle,                                         \
                  const std::initializer_list<std::size_t>& qubits,     \
                  double tol = 0.0)                                     \
  {                                                                     \
    std::string expr = #_func"(" + genQConst(angle) + ",sel_<";         \
    for (auto it=qubits.begin(); it!=qubits.end(); ++it)                \
      expr += std::to_string(*it) + (it!=qubits.end()-1 ? "," : "");    \
    expr += ">(" + _expr + "))";                                   \
    _expr = expr;                                                       \
  }                                                                     \
                                                                        \
  template<typename T,                                                  \
           typename = typename std::enable_if<std::is_base_of<QFilter,  \
                                                              typename std::decay<T>::type>::value, \
                                              void>::type>              \
  void _prog_func(double angle, const T& filter, double tol = 0.0)      \
  {                                                                     \
    std::stringstream ss;                                               \
    ss << set << filter << unset;                                       \
    _expr = #_func"<" + genQConst_t(tol) + ">(" + genQConst(angle) + "," + ss.str() + _expr + "))"; \
  }

/// @brief Macro declares QProgram member functions for a binary
/// LibKet function with no arguments, e.g.
///
/// \code{.cpp}
///   QProgram prog;
///   prog.cnot( {1,2}, {3,4} );
/// \endcode
///
/// applies a CNOT gate to the qubit pairs 1,3 and 2,4.
#define PROGRAM_ADD_BINARY(_prog_func, _func)                      \
  void _prog_func(std::size_t qubit0, std::size_t qubit1)               \
  {                                                                     \
    _prog_func({qubit0}, {qubit1});                                     \
  }                                                                     \
                                                                        \
  void _func(const std::initializer_list<std::size_t>& qubits0,         \
             const std::initializer_list<std::size_t>& qubits1)         \
  {                                                                     \
    std::string expr = #_func"(sel_<";                                  \
    for (auto it=qubits0.begin(); it!=qubits0.end(); ++it)              \
      expr += std::to_string(*it) + (it!=qubits0.end()-1 ? "," : "");   \
    expr += ">(),sel_<";                                                \
    for (auto it=qubits1.begin(); it!=qubits1.end(); ++it)              \
      expr += std::to_string(*it) + (it!=qubits1.end()-1 ? "," : "");   \
    expr += ">(" + _expr + "))";                                        \
    _expr = expr;                                                       \
  }                                                                     \
                                                                        \
  template<typename T0, typename T1,                                    \
           typename = typename std::enable_if<std::is_base_of<QFilter,  \
                                                              typename std::decay<T0>::type>::value, \
                                              void>::type,              \
           typename = typename std::enable_if<std::is_base_of<QFilter,  \
                                                              typename std::decay<T1>::type>::value, \
                                              void>::type>              \
  void _prog_func(const T0& filter0, const T1& filter1)                 \
  {                                                                     \
    std::stringstream ss0,ss1;                                          \
    ss0 << filter0;                                                     \
    ss1 << set << filter1 << unset;                                     \
    _expr = #_func"(" + ss0.str() + "," + ss1.str() + _expr + "))";     \
  }

/// @brief Macro declares QProgram member functions for a binary
/// LibKet function with one argument, e.g.
///
/// \code{.cpp}
///   QProgram prog;
///   prog.cx(3.141, {1,2}, {3,4} );
/// \endcode
///
/// applies a controlled rotation about the x-axis to qubits 3 and 4
/// controlled by qubits 1 and 2, respectively.
#define PROGRAM_ADD_BINARY_ANGLE_TOL(_prog_func, _func)                 \
  void _prog_func(double angle, std::size_t qubit0, std::size_t qubit1, double tol) \
  {                                                                     \
    _prog_func(angle, {qubit0}, {qubit1}, tol);                         \
  }                                                                     \
                                                                        \
  void _prog_func(double angle,                                         \
                  const std::initializer_list<std::size_t>& qubits0,    \
                  const std::initializer_list<std::size_t>& qubits1,    \
                  double tol = 0.0)                                     \
  {                                                                     \
    std::string expr = #_func"(" + genQConst(angle) + ",sel_<";         \
    for (auto it=qubits0.begin(); it!=qubits0.end(); ++it)              \
      expr += std::to_string(*it) + (it!=qubits0.end()-1 ? "," : "");   \
    expr += ">(),sel_(";                                                \
    for (auto it=qubits1.begin(); it!=qubits1.end(); ++it)              \
      expr += std::to_string(*it) + (it!=qubits1.end()-1 ? "," : "");   \
    expr += ">(" + _expr + "))";                                        \
    _expr = expr;                                                       \
  }                                                                     \
                                                                        \
  template<typename T0, typename T1,                                    \
           typename = typename std::enable_if<std::is_base_of<QFilter,  \
                                                              typename std::decay<T0>::type>::value, \
                                              void>::type,              \
           typename = typename std::enable_if<std::is_base_of<QFilter,  \
                                                              typename std::decay<T1>::type>::value, \
                                              void>::type>              \
  void _prog_func(double angle, const T0& filter0, const T0& filter1, double tol = 0.0) \
  {                                                                     \
    std::stringstream ss0, ss1;                                         \
    ss0 << filter0;                                                     \
    ss1 << set << filter1 << unset;                                     \
    _expr = #_func"<" + genQConst_t(tol) + ">(" + genQConst(angle) + "," + ss0.str() + "," + ss1.str() + _expr + "))"; \
  }

/// @brief Macro declares QProgram member functions for a ternary
/// LibKet function with no arguments, e.g.
///
/// \code{.cpp}
///   QProgram prog;
///   prog.ccnot( {1,2}, {3,4}, {5,6} );
/// \endcode
///
/// applies a CCNOT gate to the qubit triples 1,3,5 and 2,4,6.
#define PROGRAM_ADD_TERNARY(_prog_func, _func)                          \
  void _prog_func(std::size_t qubit0, std::size_t qubit1, std::size_t qubit2)               \
  {                                                                     \
    _prog_func({qubit0}, {qubit1}, {qubit2});                                     \
  }                                                                     \
                                                                        \
  void _func(const std::initializer_list<std::size_t>& qubits0,         \
             const std::initializer_list<std::size_t>& qubits1,         \
             const std::initializer_list<std::size_t>& qubits2)         \
  {                                                                     \
    std::string expr = #_func"(sel_<";                                  \
    for (auto it=qubits0.begin(); it!=qubits0.end(); ++it)              \
      expr += std::to_string(*it) + (it!=qubits0.end()-1 ? "," : "");   \
    expr += ">(),sel_<";                                                \
    for (auto it=qubits1.begin(); it!=qubits1.end(); ++it)              \
      expr += std::to_string(*it) + (it!=qubits1.end()-1 ? "," : "");   \
    expr += ">(),sel_<";                                                \
    for (auto it=qubits2.begin(); it!=qubits2.end(); ++it)              \
      expr += std::to_string(*it) + (it!=qubits2.end()-1 ? "," : "");   \
    expr += ">(" + _expr + "))";                                        \
    _expr = expr;                                                       \
  }                                                                     \
                                                                        \
  template<typename T0, typename T1, typename T2,                       \
           typename = typename std::enable_if<std::is_base_of<QFilter,  \
                                                              typename std::decay<T0>::type>::value, \
                                              void>::type,              \
           typename = typename std::enable_if<std::is_base_of<QFilter,  \
                                                              typename std::decay<T1>::type>::value, \
                                              void>::type,              \
           typename = typename std::enable_if<std::is_base_of<QFilter,  \
                                                              typename std::decay<T2>::type>::value, \
                                              void>::type>              \
  void _prog_func(const T0& filter0, const T1& filter1, const T1& filter2) \
  {                                                                     \
    std::stringstream ss0,ss1,ss2;                                      \
    ss0 << filter0;                                                     \
    ss1 << filter1;                                                     \
    ss2 << set << filter2 << unset;                                     \
    _expr = #_func"(" + ss0.str() + "," + ss1.str() + "," + ss2.str() + _expr + "))"; \
  }

namespace LibKet {

  /**
     @brief Program class
  */

  class QProgram
  {
  public:
    QProgram()
      : _expr("init()")
    {}
    
    void clear()
    {
      _expr = "";
    }    

    void print(std::ostream& os = std::cout) const
    {
      os << _expr;
    }

    const std::string& to_string() const
    {
      return _expr;
    }

    // Unary gates without parameters
    PROGRAM_ADD_UNARY(barrier, barrier);
    PROGRAM_ADD_UNARY(h, h);
    PROGRAM_ADD_UNARY(hdag, hdag);
    PROGRAM_ADD_UNARY(i, i);
    PROGRAM_ADD_UNARY(idag, idag);
    PROGRAM_ADD_UNARY(init, init);
    PROGRAM_ADD_UNARY(measure, measure);
    PROGRAM_ADD_UNARY(measure_x, measure_x);
    PROGRAM_ADD_UNARY(measure_y, measure_y);
    PROGRAM_ADD_UNARY(measure_z, measure_z);
    PROGRAM_ADD_UNARY(mx90, mx90);
    PROGRAM_ADD_UNARY(mx90dag, mx90dag);
    PROGRAM_ADD_UNARY(my90, my90);
    PROGRAM_ADD_UNARY(my90dag, my90dag);
    PROGRAM_ADD_UNARY(prep_x, prep_x);
    PROGRAM_ADD_UNARY(prep_y, prep_y);
    PROGRAM_ADD_UNARY(prep_z, prep_z);
    PROGRAM_ADD_UNARY(reset, reset);
    PROGRAM_ADD_UNARY(s, s);
    PROGRAM_ADD_UNARY(sdag, sdag);
    PROGRAM_ADD_UNARY(snot, snot);
    PROGRAM_ADD_UNARY(snotdag, snotdag);   
    PROGRAM_ADD_UNARY(t, t);
    PROGRAM_ADD_UNARY(tdag, tdag);
    PROGRAM_ADD_UNARY(x, x);
    PROGRAM_ADD_UNARY(x90, x90);
    PROGRAM_ADD_UNARY(x90dag, x90dag);
    PROGRAM_ADD_UNARY(xdag, xdag);
    PROGRAM_ADD_UNARY(y, y);
    PROGRAM_ADD_UNARY(y90, y90);
    PROGRAM_ADD_UNARY(y90dag, y90dag);
    PROGRAM_ADD_UNARY(ydag, ydag);
    PROGRAM_ADD_UNARY(z, z);
    PROGRAM_ADD_UNARY(zdag, zdag);

    // Unary gates with one parameter
    PROGRAM_ADD_UNARY_ANGLE_TOL(ph, ph);
    PROGRAM_ADD_UNARY_ANGLE_TOL(phdag, phdag);
    PROGRAM_ADD_UNARY_ANGLE_TOL(rx, rx);
    PROGRAM_ADD_UNARY_ANGLE_TOL(rxdag, rxdag);
    PROGRAM_ADD_UNARY_ANGLE_TOL(ry, ry);
    PROGRAM_ADD_UNARY_ANGLE_TOL(rydag, rydag);
    PROGRAM_ADD_UNARY_ANGLE_TOL(rz, rz);
    PROGRAM_ADD_UNARY_ANGLE_TOL(rzdag, rzdag);

    // Binary gates without parameter
    PROGRAM_ADD_BINARY(cnot, cnot);
    PROGRAM_ADD_BINARY(cnotdag, cnotdag);
    PROGRAM_ADD_BINARY(swap, swap);
    PROGRAM_ADD_BINARY(swapdag, swapdag);
    PROGRAM_ADD_BINARY(sswap, sswap);
    PROGRAM_ADD_BINARY(sswapdag, sswapdag);
    
    // Binary gates with one parameter
    PROGRAM_ADD_BINARY_ANGLE_TOL(cph, cph);
    PROGRAM_ADD_BINARY_ANGLE_TOL(cphdag, cphdag);
    PROGRAM_ADD_BINARY_ANGLE_TOL(cx, cx);
    PROGRAM_ADD_BINARY_ANGLE_TOL(cxdag, cxdag);
    PROGRAM_ADD_BINARY_ANGLE_TOL(cy, cy);
    PROGRAM_ADD_BINARY_ANGLE_TOL(cydag, cydag);
    PROGRAM_ADD_BINARY_ANGLE_TOL(cz, cz);
    PROGRAM_ADD_BINARY_ANGLE_TOL(czdag, czdag);

    // Ternary gates without parameter
    PROGRAM_ADD_TERNARY(ccnot, ccnot);
    PROGRAM_ADD_TERNARY(ccnotdag, ccnotdag);
    
  private:

    template<typename T>
    static inline std::string genQConst(T value)
    {
      std::stringstream ss;
      std::string str;
      
      ss << std::setprecision (std::numeric_limits<T>::digits10 + 1)
         << value;
      
      str = ss.str();
      str.erase(std::remove(str.begin(), str.end(), '0'), str.end());

      if (str.empty())
        str = "0";
      
      return "QConst"
        + std::to_string(1<<(int)std::ceil(std::log2(str.size())))
        + "(" + str + ")";
    }

    template<typename T>
    static inline std::string genQConst_t(T value)
    {
      std::stringstream ss;
      std::string str;
      
      ss << std::setprecision (std::numeric_limits<T>::digits10 + 1)
         << value;
      
      str = ss.str();
      str.erase(std::remove(str.begin(), str.end(), '0'), str.end());

      if (str.empty())
        str = "0";
      
      return "QConst"
        + std::to_string(1<<(int)std::ceil(std::log2(str.size())))
        + "_t(" + str + ")";
    }
    
    std::string _expr;
  };

  std::ostream&
  operator<<(std::ostream& os, const QProgram prog)
  {
    prog.print(os);
    return os;
  }
  
} // namespace LibKet

#endif // QPROGRAM_HPP
