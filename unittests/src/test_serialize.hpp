/** @file test_serialize.hpp
 *
 *  @brief Serialization unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, serialize)
{
  try {
    auto e = all();
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "all()");     
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = init();
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "all(init(filter()))");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
  
  try {
    auto e = hadamard();
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "hadamard(filter())");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = hadamard(sel_<0>());
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "hadamard(sel_<0>())");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = all(hadamard(sel_<0>()));
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "all(hadamard(sel_<0>()))");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = swap();
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "swap(filter(),filter())");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = swap(sel_<0>(),sel_<1>());
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "swap(sel_<0>(),sel_<1>())");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = swap(sel_<0>(),sel_<1>(init()));
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "swap(sel_<0>(),sel_<1>(init(filter())))");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = swap(sel_<0>(init()),sel_<1>());
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "swap(sel_<0>(init(filter())),sel_<1>())");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = swap(h(sel_<0>()),sel_<1>());
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "swap(hadamard(sel_<0>()),sel_<1>())");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = swap(sel_<0>(),h(sel_<1>()));
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "swap(sel_<0>(),hadamard(sel_<1>()))");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = swap(x(sel_<0>()),h(sel_<1>()));
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "swap(pauli_x(sel_<0>()),hadamard(sel_<1>()))");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = swap(all(x(sel_<0>())),h(sel_<1>()));
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "swap(all(pauli_x(sel_<0>())),hadamard(sel_<1>()))");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = swap(x(sel_<0>()),all(h(sel_<1>())));
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "swap(pauli_x(sel_<0>()),all(hadamard(sel_<1>())))");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = swap(all(x(sel_<0>())),all(h(sel_<1>())));
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "swap(all(pauli_x(sel_<0>())),all(hadamard(sel_<1>())))");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = all(swap(x(sel_<0>()),h(sel_<1>())));
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "all(swap(pauli_x(sel_<0>()),hadamard(sel_<1>())))");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = ccnot();
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "ccnot(filter(),filter(),filter())");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = ccnot(sel_<0>(),sel_<1>(),sel_<2>());
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "ccnot(sel_<0>(),sel_<1>(),sel_<2>())");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = ccnot(sel_<0>(init()),sel_<1>(),sel_<2>());
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "ccnot(sel_<0>(init(filter())),sel_<1>(),sel_<2>())");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = ccnot(sel_<0>(),sel_<1>(init()),sel_<2>());
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "ccnot(sel_<0>(),sel_<1>(init(filter())),sel_<2>())");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = ccnot(sel_<0>(),sel_<1>(),sel_<2>(init()));
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "ccnot(sel_<0>(),sel_<1>(),sel_<2>(init(filter())))");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = ccnot(h(sel_<0>()),sel_<1>(),sel_<2>());
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "ccnot(hadamard(sel_<0>()),sel_<1>(),sel_<2>())");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = ccnot(sel_<0>(),h(sel_<1>()),sel_<2>());
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "ccnot(sel_<0>(),hadamard(sel_<1>()),sel_<2>())");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = ccnot(sel_<0>(),sel_<1>(),h(sel_<2>()));
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "ccnot(sel_<0>(),sel_<1>(),hadamard(sel_<2>()))");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = ccnot(x(sel_<0>()),y(sel_<1>()),h(sel_<2>()));
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "ccnot(pauli_x(sel_<0>()),pauli_y(sel_<1>()),hadamard(sel_<2>()))");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = ccnot(all(x(sel_<0>())),y(sel_<1>()),h(sel_<2>()));
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "ccnot(all(pauli_x(sel_<0>())),pauli_y(sel_<1>()),hadamard(sel_<2>()))");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = ccnot(x(sel_<0>()),all(y(sel_<1>())),h(sel_<2>()));
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "ccnot(pauli_x(sel_<0>()),all(pauli_y(sel_<1>())),hadamard(sel_<2>()))");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = ccnot(x(sel_<0>()),y(sel_<1>()),all(h(sel_<2>())));
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "ccnot(pauli_x(sel_<0>()),pauli_y(sel_<1>()),all(hadamard(sel_<2>())))");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto e = all(ccnot(x(sel_<0>()),y(sel_<1>()),h(sel_<2>())));
    std::stringstream ss; ss << e;
    CHECK_EQUAL(ss.str(), "all(ccnot(pauli_x(sel_<0>()),pauli_y(sel_<1>()),hadamard(sel_<2>())))");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
