/** @file test_gate_measure.hpp
 *
 *  @brief LibKet::gates::measure() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_measure)
{
  TEST_API_CONSISTENCY( measure );
  TEST_API_CONSISTENCY( MEASURE );
  
  TEST_API_CONSISTENCY( QMeasure() );

  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QMeasure", measure );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QMeasure", MEASURE );
  
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QMeasure", QMeasure() );
}
