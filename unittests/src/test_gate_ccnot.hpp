/** @file test_gate_ccnot.hpp
 *
 *  @brief LibKet::gates::ccnot() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_ccnot)
{
  TEST_API_CONSISTENCY( ccnot  );
  TEST_API_CONSISTENCY( QCCNOT );
  
  try {
    // ccnot()
    auto expr = ccnot();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "TernaryQGate\n|   gate = QCCNOT\n| filter = QFilter\n|  expr0 "
                "= QFilter\n|  expr1 = QFilter\n|  expr2 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ccnot(all(),all())
    auto expr = ccnot(all(), all(), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "TernaryQGate\n|   gate = QCCNOT\n| filter = "
                "QFilterSelectAll\n|  expr0 = QFilterSelectAll\n|  expr1 = "
                "QFilterSelectAll\n|  expr2 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ccnot(sel<0,1,3,5>(), sel<2,4,6,7>(), sel<8,9,10,11>())
    auto expr =
      ccnot(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>(), sel<8, 9, 10, 11>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QCCNOT\n| filter = QFilterSelect [ 0 1 3 5 2 4 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 3 5 ]\n|  expr1 = "
      "QFilterSelect [ 2 4 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ccnot(range<0,3>(), range<4,7>(), range<8,11>())
    auto expr = ccnot(range<0, 3>(), range<4, 7>(), range<8, 11>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QCCNOT\n| filter = QFilterSelect [ 0 1 2 3 4 5 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 = "
      "QFilterSelect [ 4 5 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ccnot(qureg<0,4>(), qureg<4,4>(), qureg<8,4>())
    auto expr = ccnot(qureg<0, 4>(), qureg<4, 4>(), qureg<8, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QCCNOT\n| filter = QFilterSelect [ 0 1 2 3 4 5 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 = "
      "QFilterSelect [ 4 5 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ccnot(qubit<1>(), qubit<2>(), qubit<3>())
    auto expr = ccnot(qubit<1>(), qubit<2>(), qubit<3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "TernaryQGate\n|   gate = QCCNOT\n| filter = QFilterSelect [ 1 "
                "2 3 ]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = "
                "QFilterSelect [ 2 ]\n|  expr2 = QFilterSelect [ 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CCNOT()
    auto expr = CCNOT();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "TernaryQGate\n|   gate = QCCNOT\n| filter = QFilter\n|  expr0 "
                "= QFilter\n|  expr1 = QFilter\n|  expr2 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CCNOT(all(),all())
    auto expr = CCNOT(all(), all(), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "TernaryQGate\n|   gate = QCCNOT\n| filter = "
                "QFilterSelectAll\n|  expr0 = QFilterSelectAll\n|  expr1 = "
                "QFilterSelectAll\n|  expr2 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CCNOT(sel<0,1,3,5>(), sel<2,4,6,7>(), sel<8,9,10,11>())
    auto expr =
      CCNOT(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>(), sel<8, 9, 10, 11>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QCCNOT\n| filter = QFilterSelect [ 0 1 3 5 2 4 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 3 5 ]\n|  expr1 = "
      "QFilterSelect [ 2 4 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CCNOT(range<0,3>(), range<4,7>(), range<8,11>())
    auto expr = CCNOT(range<0, 3>(), range<4, 7>(), range<8, 11>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QCCNOT\n| filter = QFilterSelect [ 0 1 2 3 4 5 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 = "
      "QFilterSelect [ 4 5 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CCNOT(qureg<0,4>(), qureg<4,4>(), qureg<8,4>())
    auto expr = CCNOT(qureg<0, 4>(), qureg<4, 4>(), qureg<8, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QCCNOT\n| filter = QFilterSelect [ 0 1 2 3 4 5 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 = "
      "QFilterSelect [ 4 5 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CCNOT(qubit<1>(), qubit<2>(), qubit<3>())
    auto expr = CCNOT(qubit<1>(), qubit<2>(), qubit<3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "TernaryQGate\n|   gate = QCCNOT\n| filter = QFilterSelect [ 1 "
                "2 3 ]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = "
                "QFilterSelect [ 2 ]\n|  expr2 = QFilterSelect [ 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCCNOT()
    auto expr = QCCNOT();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(), "QCCNOT\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCCNOT()(all(), all(), all())
    auto expr = QCCNOT()(all(), all(), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "TernaryQGate\n|   gate = QCCNOT\n| filter = "
                "QFilterSelectAll\n|  expr0 = QFilterSelectAll\n|  expr1 = "
                "QFilterSelectAll\n|  expr2 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCCNOT()(sel<0,1,3,5>(), sel<2,4,6,7>(), sel<8,9,10,11>())
    auto expr =
      QCCNOT()(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>(), sel<8, 9, 10, 11>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QCCNOT\n| filter = QFilterSelect [ 0 1 3 5 2 4 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 3 5 ]\n|  expr1 = "
      "QFilterSelect [ 2 4 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCCNOT()(range<0,3>(), range<4,7>(), range<8,11>())
    auto expr = QCCNOT()(range<0, 3>(), range<4, 7>(), range<8, 11>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QCCNOT\n| filter = QFilterSelect [ 0 1 2 3 4 5 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 = "
      "QFilterSelect [ 4 5 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCCNOT()(qureg<0,4>(), qureg<4,4>(), qureg<8,4>())
    auto expr = QCCNOT()(qureg<0, 4>(), qureg<4, 4>(), qureg<8, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QCCNOT\n| filter = QFilterSelect [ 0 1 2 3 4 5 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 = "
      "QFilterSelect [ 4 5 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCCNOT()(qubit<1>(), qubit<2>(), qubit<3>())
    auto expr = QCCNOT()(qubit<1>(), qubit<2>(), qubit<3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "TernaryQGate\n|   gate = QCCNOT\n| filter = QFilterSelect [ 1 "
                "2 3 ]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = "
                "QFilterSelect [ 2 ]\n|  expr2 = QFilterSelect [ 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // run(ccnot(sel<0, 1, 2>(), sel<3, 4, 5>(), sel<6, 7, 8>(init())))
    auto expr = ccnot(sel<0, 1, 2>(), sel<3, 4, 5>(), sel<6, 7, 8>(init()));
    CHECK(run(expr));
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
