/** @file test_gate_hook.hpp
 *
 *  @brief LibKet::gates::hook() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_hook)
{
  TEST_API_CONSISTENCY( hook<LibKet::hook_ftor> );
  TEST_API_CONSISTENCY( HOOK<LibKet::hook_ftor> );
  TEST_API_CONSISTENCY( hookdag<LibKet::hook_ftor> );
  TEST_API_CONSISTENCY( HOOKdag<LibKet::hook_ftor> );

  TEST_API_CONSISTENCY( (hook<LibKet::hook_ftor, LibKet::hook_dag_ftor>) );
  TEST_API_CONSISTENCY( (HOOK<LibKet::hook_ftor, LibKet::hook_dag_ftor>) );
  TEST_API_CONSISTENCY( (hookdag<LibKet::hook_ftor, LibKet::hook_dag_ftor>) );
  TEST_API_CONSISTENCY( (HOOKdag<LibKet::hook_ftor, LibKet::hook_dag_ftor>) );
  
  TEST_API_CONSISTENCY( LibKet::gates::QHook<LibKet::hook_ftor>() );
  TEST_API_CONSISTENCY( (LibKet::gates::QHook<LibKet::hook_ftor, LibKet::hook_dag_ftor>()) );

  TEST_UNARY_GATE( "QHook<6953632709834,6953632709834>", hook<LibKet::hook_ftor> );
  TEST_UNARY_GATE( "QHook<6953632709834,6953632709834>", HOOK<LibKet::hook_ftor> );
  TEST_UNARY_GATE( "QHook<6953632709834,6953632709834>", hookdag<LibKet::hook_ftor> );
  //TEST_UNARY_GATE( "QHook<6953632709834,6953632709834>", HOOKdag<LibKet::hook_ftor> );

  TEST_UNARY_GATE( "QHook<6953632709834,8246394127223462504>", (hook<LibKet::hook_ftor, LibKet::hook_dag_ftor>) );
  TEST_UNARY_GATE( "QHook<6953632709834,8246394127223462504>", (HOOK<LibKet::hook_ftor, LibKet::hook_dag_ftor>) );
  //TEST_UNARY_GATE( "QHook<8246394127223462504,6953632709834>", (hookdag<LibKet::hook_ftor, LibKet::hook_dag_ftor>) );
  //TEST_UNARY_GATE( "QHook<8246394127223462504,6953632709834>", (HOOKdag<LibKet::hook_ftor, LibKet::hook_dag_ftor>) );

  TEST_UNARY_GATE( "QHook<6953632709834,6953632709834>", LibKet::gates::QHook<LibKet::hook_ftor>() );
  //TEST_UNARY_GATE( "QHook<8246394127223462504,6953632709834>", LibKet::gates::QHook<LibKet::hook_ftor, LibKet::hook_dag_ftor>() ); // cannot be tested
  
  try {
    // hook<ftor>(hook<ftor>())
    auto expr = LibKet::gates::hook<LibKet::hook_ftor>(LibKet::gates::hook<LibKet::hook_ftor>());
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QHook<6953632709834,6953632709834>\n"
                "| filter = QFilter\n"
                "|   expr = UnaryQGate\n"
                "|          |   gate = QHook<6953632709834,6953632709834>\n"
                "|          | filter = QFilter\n"
                "|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // hook<ftor,dag_ftor>(hook<dag_ftor,ftor>())
    auto expr = LibKet::gates::hook<LibKet::hook_ftor,LibKet::hook_dag_ftor>(LibKet::gates::hook<LibKet::hook_dag_ftor,LibKet::hook_ftor>());
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QHook<6953632709834,8246394127223462504>\n"
                "| filter = QFilter\n"
                "|   expr = UnaryQGate\n"
                "|          |   gate = QHook<8246394127223462504,6953632709834>\n"
                "|          | filter = QFilter\n"
                "|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // hook<ftor,dag_ftor>(hook<ftor,dag_ftor>())
    auto expr = LibKet::gates::hook<LibKet::hook_ftor, LibKet::hook_dag_ftor>(LibKet::gates::hook<LibKet::hook_ftor,LibKet::hook_dag_ftor>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QHook<6953632709834,8246394127223462504>\n"
                "| filter = QFilter\n"
                "|   expr = UnaryQGate\n"
                "|          |   gate = QHook<6953632709834,8246394127223462504>\n"
                "|          | filter = QFilter\n"
                "|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // hook<ftor>(hook<ftor>(init()))
    auto expr = LibKet::gates::hook<LibKet::hook_ftor>(LibKet::gates::hook<LibKet::hook_ftor>(init()));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QInit\n"
      "| filter = QFilterSelectAll\n"
      "|   expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n"
      "|   gate = QHook<6953632709834,6953632709834>\n"
      "| filter = QFilterSelectAll\n"
      "|   expr = UnaryQGate\n"
      "|          |   gate = QHook<6953632709834,6953632709834>\n"
      "|          | filter = QFilterSelectAll\n"
      "|          |   expr = UnaryQGate\n"
      "|          |          |   gate = QInit\n"
      "|          |          | filter = QFilterSelectAll\n"
      "|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // hook<ftor,dag_ftor>(hook<dag_ftor,ftor>(init()))
    auto expr = LibKet::gates::hook<LibKet::hook_ftor,LibKet::hook_dag_ftor>(LibKet::gates::hook<LibKet::hook_dag_ftor,LibKet::hook_ftor>(init()));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n"
      "|   gate = QInit\n"
      "| filter = QFilterSelectAll\n"
      "|   expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n"
      "|   gate = QHook<6953632709834,8246394127223462504>\n"
      "| filter = QFilterSelectAll\n"
      "|   expr = UnaryQGate\n"
      "|          |   gate = QHook<8246394127223462504,6953632709834>\n"
      "|          | filter = QFilterSelectAll\n"
      "|          |   expr = UnaryQGate\n"
      "|          |          |   gate = QInit\n"
      "|          |          | filter = QFilterSelectAll\n"
      "|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // hook<ftor,dag_ftor>(hook<ftor,dag_ftor>(init()))
    auto expr = LibKet::gates::hook<LibKet::hook_ftor,LibKet::hook_dag_ftor>(LibKet::gates::hook<LibKet::hook_ftor,LibKet::hook_dag_ftor>(init()));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n"
      "|   gate = QHook<6953632709834,8246394127223462504>\n"
      "| filter = QFilterSelectAll\n"
      "|   expr = UnaryQGate\n"
      "|          |   gate = QHook<6953632709834,8246394127223462504>\n"
      "|          | filter = QFilterSelectAll\n"
      "|          |   expr = UnaryQGate\n"
      "|          |          |   gate = QInit\n"
      "|          |          | filter = QFilterSelectAll\n"
      "|          |          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
