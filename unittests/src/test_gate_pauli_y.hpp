/** @file test_gate_pauli_y.hpp
 *
 *  @brief LibKet::gates::pauli_y() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_pauli_y)
{
  TEST_API_CONSISTENCY( pauli_y );
  TEST_API_CONSISTENCY( PAULI_Y );
  TEST_API_CONSISTENCY( y       );
  TEST_API_CONSISTENCY( Y       );

  TEST_API_CONSISTENCY( pauli_ydag );
  TEST_API_CONSISTENCY( PAULI_Ydag );
  TEST_API_CONSISTENCY( ydag       );
  TEST_API_CONSISTENCY( Ydag       );

  TEST_API_CONSISTENCY( QPauli_Y() );

  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Y", pauli_y );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Y", PAULI_Y );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Y", y       );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Y", Y       );
  
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Y", pauli_ydag );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Y", PAULI_Ydag );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Y", ydag       );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Y", Ydag       );

  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_Y", QPauli_Y() );
}
