/** @file test_gate_unitary2dag.hpp
 *
 *  @brief LibKet::gates::unitary2dag() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_unitary2dag)
{
  TEST_API_CONSISTENCY( unitary2dag<LibKet::u2_ftor> );
  TEST_API_CONSISTENCY( UNITARY2dag<LibKet::u2_ftor> );
  TEST_API_CONSISTENCY( u2dag<LibKet::u2_ftor>       );
  TEST_API_CONSISTENCY( U2dag<LibKet::u2_ftor>       );
  
  TEST_API_CONSISTENCY( QUnitary2dag<LibKet::u2_ftor>() );

  TEST_UNARY_GATE( "QUnitary2dag<4729080009990889727,QConst1_t(0)>", unitary2dag<LibKet::u2_ftor> );
  TEST_UNARY_GATE( "QUnitary2dag<4729080009990889727,QConst1_t(0)>", UNITARY2dag<LibKet::u2_ftor> );
  TEST_UNARY_GATE( "QUnitary2dag<4729080009990889727,QConst1_t(0)>", u2dag<LibKet::u2_ftor> );
  TEST_UNARY_GATE( "QUnitary2dag<4729080009990889727,QConst1_t(0)>", U2dag<LibKet::u2_ftor> );

  TEST_UNARY_GATE( "QUnitary2dag<4729080009990889727,QConst1_t(0)>", QUnitary2dag<LibKet::u2_ftor>() );
  
}
