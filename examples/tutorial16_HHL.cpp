/** @file examples/tutorial16_HHL.cpp

    @brief C++ example: Implementation of the HHL algorithm

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Huub Donkers
*/

#include <iostream>
#include <cmath>
#include <LibKet.hpp>

// Import LibKet namespaces
using namespace LibKet; 
using namespace LibKet::circuits;

// System input
template<int N>
struct U2_ftor {
  inline auto operator()() const noexcept
  { 
    arma::Mat<double> A = {{ 1.0,     -1.0/3.0},                               
                           {-1.0/3.0,  1.0}};
                      
    arma::Mat<arma::cx_double> U =
      arma::expmat((1 << N)*(3.0*arma::datum::pi/4.0)*A*arma::cx_double(0,1));                       

    return U.as_col();
  }
};

// ZYZ-decomposition
arma::vec decomposeZYZ(arma::Col<arma::cx_double> U){

  arma::Col<double> x = {0.0, 0.0, 0.0};

    if(abs(U[0]) >= abs(U[1])){
      x[1] = 2*acos(abs(U[0]));
    } else{
      x[1] = 2*acos(abs(U[1]));
    }

    double plus_term;
    double min_term; 
    if (cos(0.5*x[1]) == 0){
      plus_term = 0.0;
    }
    else {
      plus_term = 2*atan2(imag(U[3]/cos(0.5*x[1])), real(U[3]/cos(0.5*x[1])));
    }

    if (sin(0.5*x[1]) == 0){
      min_term = 0.0;
    }
    else {
      min_term =  2*atan2(imag(U[2]/sin(0.5*x[1])), real(U[2]/sin(0.5*x[1])));
    }

    x[0] = (plus_term + min_term)/2.0;
    x[2] = (plus_term - min_term)/2.0;

  return x;

}

// Quantum phase estimation loop body
template<index_t start, index_t end, index_t step, index_t index>
struct QPE_loop
{  
  template<typename Expr>
  inline constexpr auto operator()(Expr&& expr) noexcept
  { 
    return all(cu2<U2_ftor<index-1>>(sel<index>(), sel<end+1>(expr)));
  }
};

// Inverse quantum phase estimation loop body
template<index_t start, index_t end, index_t step, index_t index>
struct IQPE_loop
{  
  template<typename Expr>
  inline constexpr auto operator()(Expr&& expr) noexcept
  { 
    return all(cu2dag<U2_ftor<end-index>>(sel<end+1-index>(), sel<end+1>(expr)));
  }
};

// Rotation loop body
template<index_t start, index_t end, index_t step, index_t index>
struct RY_loop
{  
  template<typename Expr>
  inline constexpr auto operator()(Expr&& expr) noexcept
  { 
    QVar_t<index,1>  angle(2*asin(1.0/index));
    return all(cry(angle, sel<index>(), sel<0>(expr)));
  }
};

int main(int argc, char *argv[])
{
  // Set number of shots
  const size_t shots = 4096;
  
  // Set number of qubits
  const size_t b_qubits = 1;
  const size_t c_qubits = 2;

  //decomposeZYZ(U2_ftor<1>{}());

  // Initialize qubits
  auto e0 = init();

  // Prepare b register
  auto e1 = all(x(qureg<1+c_qubits, b_qubits>(e0))); 

  // Superposition of clock register
  auto e2 = all(h(qureg<1,c_qubits>(e1)));   

  // QPE
  auto e3 = all(utils::constexpr_for<1, c_qubits, 1, QPE_loop>(e2));

  // Inverse QFT
  auto e4 = all(qftdag<QFTdagMode::standard>(qureg<1, c_qubits>(e3)));

  // RY
  auto e5 = all(utils::constexpr_for<1, c_qubits, 1, RY_loop>(e4));

  // Measure ancilla
  auto e6 = all(measure(sel<0>(e5)));

  // QFT
  auto e7 = all(qft<QFTMode::standard>(qureg<1, c_qubits>(e6)));

  // Inverse QPE
  auto e8 = all(utils::constexpr_for<1, c_qubits, 1, IQPE_loop>(e7));

  // Superposition of clock register
  auto e9 = all(h(qureg<1,c_qubits>(e8)));  

  // Measure b register
  auto e10 = all(measure(qureg<1+c_qubits, b_qubits>(e9))); 
  
  // Create empty JSON object to receive results
  utils::json result;

  // Create device
  QDevice<QDeviceType::qiskit_aer_simulator, 1+c_qubits+b_qubits> qpu;

  qpu(e10);
  result = qpu.eval(shots);

  auto hist = qpu.get<QResultType::histogram>(result);

  float b0_count = hist[1];
  float b1_count = hist[9];
  float b0_prob = b0_count/float(shots);
  float b1_prob = b1_count/float(shots); 

  std::cout << "b0 prob: " << b0_prob << std::endl;
  std::cout << "b1 prob: " << b1_prob << std::endl;
  std::cout << "Ratio: " << b1_prob/b0_prob << std::endl;

  return 0;
}
