/** @file examples/tutorial17_vqe.cpp

    @brief C++ example: Implementation of the VQE algorithm

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Huub Donkers
*/

#include <iostream>
#include <LibKet.hpp>
#include <nlopt.hpp>

// Import LibKet namespaces
using namespace LibKet; 
using namespace LibKet::circuits;

/* This example code implementes the basic VQE functionality where
 * a classical optimizer attempts to minimize the expectation value
 * of a given quantum subroutine. In this case, the Hamiltonian 
 * H = c_0 X_0 I_1 + c_1 I_0 X_1 will be evaluated. The ansatz used
 * in this example is the 2 qubit U3 ansatz, needing 24 optimization
 * parameters. Altough the Pauli terms of the Hamiltonian are static,
 * the coefficients can be altered in the Hamiltonian coeffienct struct
 */

// Hamiltonian coefficients structure
struct HamiltonianCoeffs
{
  double c[2] = {0.5, 0.6};
};

// VQE structure
struct VQE
{
  template<int index, typename Expr>
  auto U3(Expr&& expr, double theta, double phi, double lambda)
  {    
    QVar_t<3*index,1>   var_theta(theta*2*M_PI);
    QVar_t<3*index+1,1> var_phi(phi*2*M_PI);
    QVar_t<3*index+2,1> var_lambda(lambda*2*M_PI);

    return rz(var_phi,
              rx(-QConst_M_PI_2, 
                 rz(var_theta,
                    rx(QConst_M_PI_2, 
                       rz(var_lambda, 
                          expr
                          )
                       )
                    )
                 )
              );
  }

  template<typename Expr, typename Params>
  auto genAnsatz(Expr&& expr, Params&& params){
     
    // 2-qubit U3 ansatz         
    auto U3_0 = U3<0>(sel<0>(init()), params[0], params[1], params[2]);
    auto U3_1 = U3<1>(sel<1>(all(U3_0)), params[3], params[4], params[5]);

    auto CNOT1 = cnot(sel<1>(), sel<0>(all(U3_1)));

    auto U3_2 = U3<2>(sel<0>(all(CNOT1)), params[6], params[7], params[8]);
    auto U3_3 = U3<3>(sel<1>(all(U3_2)), params[9], params[10], params[11]);

    auto CNOT2 = cnot(sel<0>(), sel<1>(all(U3_3)));

    auto U3_4 = U3<4>(sel<0>(all(CNOT2)), params[12], params[13], params[14]);
    auto U3_5 = U3<5>(sel<1>(all(U3_4)), params[15], params[16], params[17]);

    auto CNOT3 = cnot(sel<1>(), sel<0>(all(U3_5)));

    auto U3_6 = U3<6>(sel<0>(all(CNOT3)), params[18], params[19], params[20]);
    auto U3_7 = U3<7>(sel<1>(all(U3_6)), params[21], params[22], params[23]);

    return all(U3_7);
  }

  // Convert histogram output to expectation value
  template<typename HistType>
  double histToExp(HistType hist){

    // Initialize expectation value an total number of shots to zero
    long expectation = 0;
    long shots = 0;
      
    //Loop over all states
    for(int state = 0; state < hist.size(); state++)
      {
        // Get bitset based on hist index
        std::bitset<2> bits(state);
        
        // Add expectations bases on bitset parity
        if(bits.count() % 2 == 0)
          { // Even
            expectation += (long)hist[state];
          }
        else{
          // Odd
          expectation -= (long)hist[state];
      }
      shots += hist[state];
    }
    // Return expectation over all shots
    return (double)expectation/(double)shots;
  }
  
  template<QDeviceType DeviceType, typename Params, typename H_coeffs>
  double runVQE(Params&& params, H_coeffs h_coeffs, std::size_t shots)
  {     
    // Generate ansatz (without measurements)
    auto anz = genAnsatz(init(), params);

    // Initalize two quantum devices, each for a measurement basis
    QDevice<DeviceType, 2> qpu0, qpu1;

    // Load ansatz onto QPU and execute
    qpu0(measure(h(sel<0>(anz))));                                   // Measure qubit 0 in X basis
    auto result0 = qpu0.eval(shots);                                 // Execute for shots times
    auto hist0 = qpu0.template get<QResultType::histogram>(result0); // Retrieve histogram from results
    auto expectation0 = histToExp(hist0);                            // Get expectation value from histogram

    qpu1(measure(h(sel<1>(anz))));                                   // Measure qubit 1 in X basis
    auto result1 = qpu1.eval(shots);                                 // Execute for shots times
    auto hist1 = qpu1.template get<QResultType::histogram>(result1); // Retrieve histogram from results
    auto expectation1 = histToExp(hist1);                            // Get expectation value from histogram

    //Return expectation value for each Hamiltonian term, scaled by its coeffient
    return h_coeffs.c[0]*expectation0 + h_coeffs.c[1]*expectation1;
  }
};

// NLopt optimization function
static double vqa_opt(const std::vector<double> &params, std::vector<double> &grad, void *func_data)
{
  static std::size_t iter=0;
  
  // Create Hamiltonian
  HamiltonianCoeffs H_coeffs;

  // Create VQE struct
  VQE vqe;

  // Run VQE and get expectation function
  double expectation = vqe.runVQE<QDeviceType::qiskit_aer_simulator>(params, H_coeffs, 4096);

  // Iteration message:
  QInfo << iter++ << " " << expectation;
  for (std::size_t i=0; i<params.size(); ++i)
    QInfo << " " << params[i];
  QInfo << std::endl;

  return expectation;
}

// Compute classical solution
double classicalExpectation(){

  // Complex contants (0, i, 1)
  std::complex<double> c_0(0,0);
  std::complex<double> c_i(0,1);
  std::complex<double> c_1(1,0);

  HamiltonianCoeffs H_coeffs;

  // Pauli matrices
  arma::cx_mat I = {{c_1,  c_0}, {c_0,  c_1}};
  arma::cx_mat X = {{c_0,  c_1}, {c_1,  c_0}};
  arma::cx_mat H_BK = H_coeffs.c[0]*arma::kron(X, I) + H_coeffs.c[1]*arma::kron(I, X);
  arma::vec eigval = arma::sort(arma::eig_sym(real(H_BK)));

  return eigval[0];
}   


int main(int argc, char *argv[])
{  
  // Set U3 parameters   
  std::size_t numParams = 8*3;

  // Set NLopt variables     
  double _minf;
  std::vector<double> _opt_params;

  // NLopt optimizer initialization    
  std::vector<double> lb;
  std::vector<double> ub;
  std::vector<double> params;
  double tol = 1e-3;
   
  // Fill in parameters based on algorithm   
  for(int i=0; i < numParams; i++)
    {
      lb.push_back(0.0);      // Set lower bounds to 0.0
      ub.push_back(1.0);      // Set upper bounds to 1.0
      params.push_back(0.5);  // Set initial pararms to 0.5
    } 

  struct OptData{} optData;

  // Set parameters
  nlopt::opt opt(nlopt::LN_COBYLA, numParams);
  opt.set_lower_bounds(lb);
  opt.set_upper_bounds(ub);
  opt.set_min_objective(vqa_opt, &optData); 
  opt.set_xtol_rel(tol);   
  
  try
    {
      nlopt::result result = opt.optimize(params, _minf);
      _opt_params = params;
    }
  catch(std::exception &e)
    {
      QInfo << "NLopt failed: " << e.what() << std::endl;
    }
  
  QInfo << "VQE Expectation: " << _minf << std::endl;
  QInfo << "Exact Expectation: " << classicalExpectation() << std::endl;
  
  return 0;
}
