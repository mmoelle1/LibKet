/** @file examples/debug.cpp

    @brief C++ example: Debugging file

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Huub Donkers
*/

#include <iostream>

#include <LibKet.hpp>

// Import LibKet namespaces
using namespace LibKet; 
using namespace LibKet::circuits;

struct U2_ftor {
  inline auto operator()() const noexcept
  {
    arma::Mat<double> A = {{1.0/sqrt(2.0), 1.0/sqrt(2.0)},                               
                           {1.0/sqrt(2.0), - 1.0/sqrt(2.0)}};             

    return A;
  }
};

int main(int argc, char *argv[])
{
  // Set number of qubits
  const size_t nqubits = 6;

  // Create quantum expression 
  auto expr0 = all(u2<U2_ftor>(sel<0>(init())));
  auto expr1 = all(u2dag<U2_ftor>(sel<1>(expr0)));
  auto expr2 = all(cu2<U2_ftor>(sel<2>(), sel<3>(expr1)));
  auto expr3 = all(cu2dag<U2_ftor>(sel<4>(), sel<5>(expr2)));
  
  // Create empty JSON object to receive results
  utils::json result;

  QDevice<QDeviceType::cirq_simulator, nqubits> qpu;

  qpu(measure(h(init())));
  result = qpu.eval(1024);

  std::cout << qpu.print_circuit() << std::endl;
  std::cout << qpu.get<QResultType::histogram>(result) << std::endl;

  auto g1 = utils::make_regular_graph<1>();
  auto g2 = utils::make_regular_graph<2>();
  auto g3 = utils::make_regular_graph<3>();
  auto g4 = utils::make_regular_graph<4>();
  auto g5 = utils::make_regular_graph<5>();
  auto g6 = utils::make_regular_graph<6>();

  std::cout << g1 << "\n"
            << g2 << "\n"
            << g3 << "\n"
            << g4 << "\n"
            << g5 << "\n"
            << g6 << std::endl;

  return 0;
}
