/** @file examples/tutorial05_qpu_execution.cpp

    @brief C++ tutorial-05: Quantum executions methods

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Huub Donkers
*/

#include <iostream>
#include <LibKet.hpp>

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

int main(int argc, char *argv[])
{
  // Set number of qubits and shots
  const std::size_t nqubits = 4;
  const std::size_t shots = 1024;

  //Contstruct simple quantum expression
  auto expr = measure(all(y(sel<0,2>(x(init())))));

  //Create new quantum device and load quantum expression into it
  QDevice<QDeviceType::qiskit_aer_simulator, nqubits> qpu;
  qpu(expr);

  //Create json objects to store results
  utils::json async_result;
  utils::json sync_result;
  utils::json eval_result;
  
  //Asynchronous execution
  auto async_job = qpu.execute_async(shots);      //Runs qpu in background
  std::cout << "Doing some stuff" << std::endl;   //Code to run while qpu is executing
  async_result = async_job->get();                //Collect results, wait if not finished yet 

  //Synchronous execution
  auto sync_job = qpu.execute(shots);             //Runs qpu in background
  std::cout << "Doing other stuff" << std::endl;  //Code is run after qpu finishes exection
  sync_result = sync_job->get();                  //Collect results

  //Evaluation
  eval_result = qpu.eval(shots);                  //Wait for qpu to finish and return results

  //Print results
  std::cout << async_result.dump(2) << std::endl;
  std::cout << sync_result.dump(2)  << std::endl;
  std::cout << eval_result.dump(2)  << std::endl;
    
  return 0;
}
