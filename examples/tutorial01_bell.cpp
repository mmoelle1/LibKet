/** @file examples/tutorial01_bell.cpp

    @brief C++ tutorial-01: Quantum filters, gates, and simple expressions

    This tutorial illustrates the basic usage of quantum filters and
    quantum gates to compose simple quantum expressions that are
    evaluated on all selected quantum backends

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#include <iostream>
#include <LibKet.hpp>
#include "common.h"

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

int main(int argc, char *argv[])
{  
  // Set number of qubits
  const size_t nqubits = 2;
    
  // Create simple quantum expression:
  // 1. Apply Hadamard gate to first qubit
  // 2. Apply controlled-not gate to q1 with controlling qubits q0
  // 3. Measure results
  auto expr = measure(
                      all(
                          cnot(
                               sel<0>(),
                               sel<1>(
                                      all(
                                          h(sel<0>(
                                              init()
                                              )
                                          )
                                         )
                                      )
                               )
                          )
                       );
  
   
  // Create empty JSON object to receive results
  utils::json result;

  // Evaluate quantum expression on multiple devices
  EVAL_EXPR_SIMPLE(nqubits, expr, 1024, result);
  
  QDebug << "Total number of jobs run: "
         << _qstream_python.size() << " Python jobs, "
         << _qstream_cxx.size() << " C++ jobs\n";
  
  return 0;
}
